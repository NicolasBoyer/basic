import { defineConfig } from 'vite'

export default defineConfig(({ mode }) => ({
	base: '',
    build: {
        outDir: `${mode}`,
        sourcemap: mode !== 'modeling/gen/site.doss/lib-md',
        target: "es2022",
        minify: true,
        rollupOptions: {
            input: 'modeling/gen/site.ts.doss/lib-md/index.ts',
            output: {
				assetFileNames: 'styles/[name]-[hash][extname]',
				chunkFileNames: '[name].js',
                entryFileNames: 'index.js'
            }
        }
    }
}))
