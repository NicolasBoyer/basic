package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class BCol_head(override var parent: BTable_, node: Element?) : TextCol<INone>(parent, node) {
	override val role: String get() = "head"
}
