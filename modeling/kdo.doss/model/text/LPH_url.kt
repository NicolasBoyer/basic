package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class LPH_url(override var parent: IMaterial /* BPara | BTblCaption | LIS_emp | LIS_spec | LIS_sub | LIS_sup | LPH_quote | LPH_url | LRef_page */, node: Element?) : TextPhrase<Dba_url, ITextInline /* LIS_emp | LIS_spec | LIS_sub | LIS_sup | LImg_ico | LPH_quote | LPH_url | LRef_page */>(parent, node) {
	override val role: String get() = "url"
}
