package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class BExt_img(override var parent: IMaterial /* BLI | BTD_ | BTD_num | BTD_word | Tba_txt */, node: Element?) : TextExtBlock<Dba_resM, Bpng_gif_jpg_jpeg>(parent, node, allowedBExt_img) {
	override val role: String get() = "img"
}
private val allowedBExt_img = arrayOf<Class<out IMaterial>>(Bpng_gif_jpg_jpeg::class.java)
