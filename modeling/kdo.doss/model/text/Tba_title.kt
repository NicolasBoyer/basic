package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class Tba_title(override val parent: FOtitle, node: Element?) : TextMonoPara<ITextInline /* TextChars | LPH_url | LPH_quote | LIS_spec | LIS_emp | LIS_sup | LIS_sub | LImg_ico */>(parent, node) {
	override val tag: String get() = "ba:title"
}
