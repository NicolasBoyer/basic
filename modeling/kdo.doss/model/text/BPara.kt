package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class BPara(override var parent: IMaterial /* BLI | BTD_ | BTD_num | BTD_word | Tba_txt */, node: Element?) : TextPara<INone, ITextInline /* LIS_emp | LIS_spec | LIS_sub | LIS_sup | LImg_ico | LPH_quote | LPH_url | LRef_page */>(parent, node) {
}
