package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class BTR_(override var parent: BTable_, node: Element?) : TextRow<INone, TextCell<out IMeta, ITextBlock> /* BTD_ | BTD_num | BTD_word */>(parent, node) {
	override val role: String get() = ""
}
