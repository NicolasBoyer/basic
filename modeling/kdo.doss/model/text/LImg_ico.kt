package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class LImg_ico(override var parent: IMaterial /* BPara | BTblCaption | LIS_emp | LIS_spec | LIS_sub | LIS_sup | LPH_quote | LPH_url | LRef_page */, node: Element?) : TextInlineImg<INone, Bpng_gif_jpg_jpeg>(parent, node, allowedLImg_ico) {
	override val role: String get() = "ico"
}
private val allowedLImg_ico = arrayOf<Class<out IMaterial>>(Bpng_gif_jpg_jpeg::class.java)
