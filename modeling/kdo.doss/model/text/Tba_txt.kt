package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class Tba_txt(override val parent: IMaterial /* FOdesc | Pemphasis | Pfirst | Pinfo | Pnext | Ptext */, node: Element?) : TextMultiPara<ITextBlock /*BPara | BIL_ | BOL_ | BTable_ | BExt_img*/>(parent, node) {
	override val tag: String get() = "ba:txt"
}
