package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class LRef_page(override var parent: IMaterial /* BPara | BTblCaption | LIS_emp | LIS_spec | LIS_sub | LIS_sup | LPH_quote | LPH_url | LRef_page */, node: Element?) : TextUlinkRef<INone, ITextInline /* LIS_emp | LIS_spec | LIS_sub | LIS_sup | LImg_ico | LPH_quote | LPH_url | LRef_page */, Cba_page>(parent, node, allowedLRef_page) {
	override val role: String get() = "page"
}
private val allowedLRef_page = arrayOf<Class<out IMaterial>>(Cba_page::class.java)
