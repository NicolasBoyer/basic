package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class BTD_(override var parent: IMaterial /* BTR_ | BTR_head */, node: Element?) : TextCell<INone, ITextBlock /* BExt_img | BIL_ | BOL_ | BPara | BTable_ */>(parent, node) {
	override val role: String get() = ""
}
