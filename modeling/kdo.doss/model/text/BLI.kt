package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class BLI(override var parent: IMaterial /* BIL_ | BOL_ */, node: Element?) : TextListItem<INone, ITextBlock /* BExt_img | BIL_ | BOL_ | BPara | BTable_ */>(parent, node) {
}
