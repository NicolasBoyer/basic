package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class BTable_(override var parent: IMaterial /* BLI | BTD_ | BTD_num | BTD_word | Tba_txt */, node: Element?) : TextTable<INone>(parent, node) {
	override val role: String get() = ""
	override var caption: BTblCaption? = null
	override var cols = ArrayList<TextCol<out IMeta> /* BCol_ | BCol_head */>()
	override var rows = ArrayList<TextRow<out IMeta, TextCell<out IMeta, ITextBlock>> /* BTR_ | BTR_head */>()
}
