package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class BIL_(override var parent: IMaterial /* BLI | BTD_ | BTD_num | BTD_word | Tba_txt */, node: Element?) : TextItemizedList<INone, TextListItem<out IMeta, ITextBlock /* BExt_img | BIL_ | BOL_ | BPara | BTable_ */>>(parent, node) {
	override val role: String get() = ""
}
