package model.text
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.data.*
import org.w3c.dom.Element

class BTblCaption(override var parent: BTable_, node: Element?) : TextCaption<ITextInline /* LIS_emp | LIS_spec | LIS_sub | LIS_sup | LImg_ico | LPH_quote | LPH_url | LRef_page */>(parent, node) {
}
