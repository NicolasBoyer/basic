package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Ptile(parent: Cba_tiles, node: Element?): PartExt<Dba_tileM, Cba_page, Cba_tiles>("sp:tile", parent, node, allowedPtile)

private val allowedPtile = arrayOf<Class<out IMaterial>>(Cba_page::class.java)

