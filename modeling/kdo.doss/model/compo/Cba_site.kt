package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_site(override val parent: IMaterialRoot, node: Element?) : Material(parent, node), IComposition, OPhome, RSpage {
	override val tag: String get() = "ba:site"
	override var meta: Dba_siteM? = null
	override var p_home: Phome? = null
	override var s_page: CompSet<Ppage> = CompSet()

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		p_home?.let { cb(it)?.let { return it } }
		s_page.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		p_home?.let {cb(it)}
		s_page.forEach(cb)
	}
}
