package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Ppage(parent: Cba_site, node: Element?): PartExt<INone, Cba_page, Cba_site>("sp:page", parent, node, allowedPpage)

private val allowedPpage = arrayOf<Class<out IMaterial>>(Cba_page::class.java)

