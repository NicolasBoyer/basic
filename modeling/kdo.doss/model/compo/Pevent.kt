package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pevent(parent: Cba_calendar, node: Element?): PartInt<Dba_calendarEventM, INone, Cba_calendar>("sp:event", parent, node)

