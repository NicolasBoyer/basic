package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pinput(parent: Cba_form, node: Element?): PartInt<Dba_inputM, INone, Cba_form>("sp:input", parent, node)

