package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pvisavis(parent: Cba_texts, node: Element?): PartInt<Dba_blocM, Cba_visavis, Cba_texts>("sp:visavis", parent, node)

