package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pcalendar(parent: Cba_page, node: Element?): PartDep<INone, Cba_calendar, Cba_page>("sp:calendar", parent, node, allowedPcalendar)

private val allowedPcalendar = arrayOf<Class<out IMaterial>>(Cba_calendar::class.java)

