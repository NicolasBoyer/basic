package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pnext(parent: Cba_visavis, node: Element?): PartInt<INone, Tba_txt, Cba_visavis>("sp:next", parent, node)

interface RPnext : IMaterial {
	val p_next: Pnext
}
