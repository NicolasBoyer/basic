package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

interface RSvideos : IMaterial {
	val s_videos: CompSet<Pvideo>
}
