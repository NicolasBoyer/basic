package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_tiles(override val parent: IMaterial /* IMaterialRoot | Ptiles */, node: Element?) : Material(parent, node), IComposition, RStile {
	override val tag: String get() = "ba:tiles"
	override var meta: Dba_tilesM? = null
	override var s_tile: CompSet<Ptile> = CompSet()

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		s_tile.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		s_tile.forEach(cb)
	}
}
