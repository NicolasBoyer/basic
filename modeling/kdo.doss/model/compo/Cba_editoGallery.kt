package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_editoGallery(override val parent: IMaterial /* IMaterialRoot | PeditoGallery */, node: Element?) : Material(parent, node), IComposition, RSzones {
	override val tag: String get() = "ba:editoGallery"
	override var meta: Dba_editoGalleryM? = null
	override var s_zones: CompSet<Pzone> = CompSet()

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		s_zones.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		s_zones.forEach(cb)
	}
}
