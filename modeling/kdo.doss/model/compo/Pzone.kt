package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pzone(parent: Cba_editoGallery, node: Element?): PartInt<INone, Cba_slideshow, Cba_editoGallery>("sp:zone", parent, node)

