package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pfirst(parent: Cba_visavis, node: Element?): PartInt<INone, Tba_txt, Cba_visavis>("sp:first", parent, node)

interface RPfirst : IMaterial {
	val p_first: Pfirst
}
