package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pimages(parent: Cba_page, node: Element?): PartDep<Dba_imagesM, Cba_imagesGallery, Cba_page>("sp:images", parent, node, allowedPimages)

private val allowedPimages = arrayOf<Class<out IMaterial>>(Cba_imagesGallery::class.java)

