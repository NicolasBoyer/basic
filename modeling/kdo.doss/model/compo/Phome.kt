package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Phome(parent: Cba_site, node: Element?): PartExt<INone, IMaterial /* Bpng_gif_jpg_jpeg | Cba_imagesGallery */, Cba_site>("sp:home", parent, node, allowedPhome)

private val allowedPhome = arrayOf<Class<out IMaterial>>(Bpng_gif_jpg_jpeg::class.java, Cba_imagesGallery::class.java)

interface OPhome : IMaterial {
	val p_home: Phome?
}
