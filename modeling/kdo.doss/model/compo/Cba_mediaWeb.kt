package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_mediaWeb(override val parent: IMaterial /* IMaterialRoot | Pvideo */, node: Element?) : Material(parent, node), IComposition, RPunknown {
	override val tag: String get() = "ba:mediaWeb"
	override var meta: Dba_mediaWebM? = null
	override lateinit var p_unknown: Punknown

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		cb(p_unknown)?.let { return it }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		cb(p_unknown)
	}
}
