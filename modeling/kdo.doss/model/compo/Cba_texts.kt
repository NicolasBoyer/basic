package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_texts(override val parent: IMaterial /* IMaterialRoot | Ptexts */, node: Element?) : Material(parent, node), IComposition, OSblock {
	override val tag: String get() = "ba:texts"
	override var meta: Dba_textsM? = null
	override var s_block: CompSet<IPart /* Pemphasis | Pinfo | Pvisavis */>? = null

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		s_block?.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		s_block?.forEach(cb)
	}
}
