package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_form(override val parent: IMaterial /* IMaterialRoot | Pform */, node: Element?) : Material(parent, node), IComposition, RSinput {
	override val tag: String get() = "ba:form"
	override var meta: Dba_formM? = null
	override var s_input: CompSet<Pinput> = CompSet()

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		s_input.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		s_input.forEach(cb)
	}
}
