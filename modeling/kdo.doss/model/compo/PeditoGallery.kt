package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class PeditoGallery(parent: Cba_mapGallery, node: Element?): PartInt<INone, Cba_editoGallery, Cba_mapGallery>("sp:editoGallery", parent, node)

interface OPeditoGallery : IMaterial {
	val p_editoGallery: PeditoGallery?
}
