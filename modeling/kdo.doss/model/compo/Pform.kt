package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pform(parent: Cba_page, node: Element?): PartDep<INone, Cba_form, Cba_page>("sp:form", parent, node, allowedPform)

private val allowedPform = arrayOf<Class<out IMaterial>>(Cba_form::class.java)

