package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_calendar(override val parent: IMaterial /* IMaterialRoot | Pcalendar */, node: Element?) : Material(parent, node), IComposition, RSevent {
	override val tag: String get() = "ba:calendar"
	override var meta: Dba_calendarM? = null
	override var s_event: CompSet<Pevent> = CompSet()

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		s_event.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		s_event.forEach(cb)
	}
}
