package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_slideshow(override val parent: IMaterial /* IMaterialRoot | Pzone */, node: Element?) : Material(parent, node), IComposition, RSslides {
	override val tag: String get() = "ba:slideshow"
	override var meta: Dba_slideshowM? = null
	override var s_slides: CompSet<IPart /* Pimage | Ptext */> = CompSet()

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		s_slides.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		s_slides.forEach(cb)
	}
}
