package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_imagesGallery(override val parent: IMaterial /* IMaterialRoot | Pgallery | Phome | Pimages | Pmembers | Pslider */, node: Element?) : Material(parent, node), IComposition, RSimages {
	override val tag: String get() = "ba:imagesGallery"
	override var meta: Dba_imagesGalleryM? = null
	override var s_images: CompSet<Pimage> = CompSet()

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		s_images.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		s_images.forEach(cb)
	}
}
