package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

interface RSevent : IMaterial {
	val s_event: CompSet<Pevent>
}
