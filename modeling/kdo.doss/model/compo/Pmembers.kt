package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pmembers(parent: Cba_page, node: Element?): PartDep<INone, Cba_imagesGallery, Cba_page>("sp:members", parent, node, allowedPmembers)

private val allowedPmembers = arrayOf<Class<out IMaterial>>(Cba_imagesGallery::class.java)

