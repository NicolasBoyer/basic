package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pemphasis(parent: Cba_texts, node: Element?): PartInt<Dba_blocM, Tba_txt, Cba_texts>("sp:emphasis", parent, node)

