package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pvideos(parent: Cba_page, node: Element?): PartDep<INone, Cba_videosGallery, Cba_page>("sp:videos", parent, node, allowedPvideos)

private val allowedPvideos = arrayOf<Class<out IMaterial>>(Cba_videosGallery::class.java)

