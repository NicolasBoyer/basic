package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Ptiles(parent: Cba_page, node: Element?): PartDep<INone, Cba_tiles, Cba_page>("sp:tiles", parent, node, allowedPtiles)

private val allowedPtiles = arrayOf<Class<out IMaterial>>(Cba_tiles::class.java)

