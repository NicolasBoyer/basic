package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Punknown(parent: Cba_mediaWeb, node: Element?): PartExt<Dba_mwUnM, INone, Cba_mediaWeb>("sp:unknown", parent, node, allowedPunknown)

private val allowedPunknown = arrayOf<Class<out IMaterial>>()

interface RPunknown : IMaterial {
	val p_unknown: Punknown
}
