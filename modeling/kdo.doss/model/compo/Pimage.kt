package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pimage(parent: IComposition /* Cba_imagesGallery | Cba_slideshow */, node: Element?): PartExt<INone, Bpng_gif_jpg_jpeg, IComposition /* Cba_imagesGallery | Cba_slideshow */>("sp:image", parent, node, allowedPimage)

private val allowedPimage = arrayOf<Class<out IMaterial>>(Bpng_gif_jpg_jpeg::class.java)

