package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_videosGallery(override val parent: IMaterial /* IMaterialRoot | Pvideos */, node: Element?) : Material(parent, node), IComposition, RSvideos {
	override val tag: String get() = "ba:videosGallery"
	override var meta: Dba_videosGalleryM? = null
	override var s_videos: CompSet<Pvideo> = CompSet()

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		s_videos.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		s_videos.forEach(cb)
	}
}
