package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_page(override val parent: IMaterial /* IMaterialRoot | LRef_page | Ppage | Ptile */, node: Element?) : Material(parent, node), IComposition, OPslider, RS1 {
	override val tag: String get() = "ba:page"
	override var meta: Dba_pageM? = null
	override var p_slider: Pslider? = null
	override var s_1: CompSet<IPart /* Pcalendar | Pform | Pimages | PmapGallery | Pmembers | Ptexts | Ptiles | Pvideos */> = CompSet()

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		p_slider?.let { cb(it)?.let { return it } }
		s_1.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		p_slider?.let {cb(it)}
		s_1.forEach(cb)
	}
}
