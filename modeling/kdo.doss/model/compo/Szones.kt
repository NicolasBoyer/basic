package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

interface RSzones : IMaterial {
	val s_zones: CompSet<Pzone>
}
