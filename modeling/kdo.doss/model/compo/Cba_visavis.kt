package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_visavis(override val parent: IMaterial /* IMaterialRoot | Pvisavis */, node: Element?) : Material(parent, node), IComposition, RPfirst, RPnext {
	override val tag: String get() = "ba:visavis"
	override var meta: Dba_visavisM? = null
	override lateinit var p_first: Pfirst
	override lateinit var p_next: Pnext

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		cb(p_first)?.let { return it }
		cb(p_next)?.let { return it }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		cb(p_first)
		cb(p_next)
	}
}
