package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

interface RS1 : IMaterial {
	val s_1: CompSet<IPart /* Pcalendar | Pform | Pimages | PmapGallery | Pmembers | Ptexts | Ptiles | Pvideos */>
}
