package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Ptext(parent: Cba_slideshow, node: Element?): PartInt<INone, Tba_txt, Cba_slideshow>("sp:text", parent, node)

