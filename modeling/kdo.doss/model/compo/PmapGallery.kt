package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class PmapGallery(parent: Cba_page, node: Element?): PartDep<Dba_imagesM, Cba_mapGallery, Cba_page>("sp:mapGallery", parent, node, allowedPmapGallery)

private val allowedPmapGallery = arrayOf<Class<out IMaterial>>(Cba_mapGallery::class.java)

