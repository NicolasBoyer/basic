package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Cba_mapGallery(override val parent: IMaterial /* IMaterialRoot | PmapGallery */, node: Element?) : Material(parent, node), IComposition, OPgallery, OPeditoGallery {
	override val tag: String get() = "ba:mapGallery"
	override var meta: Dba_mapGalleryM? = null
	override var p_gallery: Pgallery? = null
	override var p_editoGallery: PeditoGallery? = null

	override fun <T> inParts(cb: (s: IPart) -> T?): T? {
		p_gallery?.let { cb(it)?.let { return it } }
		p_editoGallery?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofParts(cb: (s: IPart) -> Unit) {
		p_gallery?.let {cb(it)}
		p_editoGallery?.let {cb(it)}
	}
}
