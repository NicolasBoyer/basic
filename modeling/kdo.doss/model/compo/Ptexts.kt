package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Ptexts(parent: Cba_page, node: Element?): PartDep<INone, Cba_texts, Cba_page>("sp:texts", parent, node, allowedPtexts)

private val allowedPtexts = arrayOf<Class<out IMaterial>>(Cba_texts::class.java)

