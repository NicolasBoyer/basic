package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pgallery(parent: Cba_mapGallery, node: Element?): PartInt<INone, Cba_imagesGallery, Cba_mapGallery>("sp:gallery", parent, node)

interface OPgallery : IMaterial {
	val p_gallery: Pgallery?
}
