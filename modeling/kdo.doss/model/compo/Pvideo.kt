package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pvideo(parent: Cba_videosGallery, node: Element?): PartExt<INone, IMaterial /* Bmp4_f4v_ogv_webm | Cba_mediaWeb */, Cba_videosGallery>("sp:video", parent, node, allowedPvideo)

private val allowedPvideo = arrayOf<Class<out IMaterial>>(Bmp4_f4v_ogv_webm::class.java, Cba_mediaWeb::class.java)

