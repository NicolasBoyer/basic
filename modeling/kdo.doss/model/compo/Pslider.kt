package model.compo
import eu.scenari.kdo.material.*
import model.binary.*
import model.data.*
import model.text.*
import org.w3c.dom.Element

class Pslider(parent: Cba_page, node: Element?): PartExt<Dba_sliderM, Cba_imagesGallery, Cba_page>("sp:slider", parent, node, allowedPslider)

private val allowedPslider = arrayOf<Class<out IMaterial>>(Cba_imagesGallery::class.java)

interface OPslider : IMaterial {
	val p_slider: Pslider?
}
