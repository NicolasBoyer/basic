package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_videoM(override val parent: MaterialRoot, node: Element?) : Material(parent, node), IDataform, OFOtitle, OFGaccessibility {
	override val tag: String get() = "ba:videoM"
	override var f_title: FOtitle? = null
	override var f_accessibility: FGaccessibility? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		f_title?.let { cb(it)?.let { return it } }
		f_accessibility?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		f_title?.let {cb(it)}
		f_accessibility?.let {cb(it)}
	}
}
