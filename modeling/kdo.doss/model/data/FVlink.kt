package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVlink(override val parent: Dba_calendarEventM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:link", parent, node, value)

interface OFVlink : IMaterial {
	val f_link: FVlink?
}
