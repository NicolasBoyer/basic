package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVtitle(override val parent: IMaterial /* Dba_calendarEventM | Dba_imageM | Dba_sectionM | Dba_siteM | Dba_url | FGsocialeNetwork */, node: Element?, value: String = defaultVal(node)): FieldValue("sp:title", parent, node, value)

interface OFVtitle : IMaterial {
	val f_title: FVtitle?
}
interface RFVtitle : OFVtitle {
	override val f_title: FVtitle
}
