package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVisJourney(override val parent: Dba_imagesM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:isJourney", parent, node, value) {
	val option: EFVisJourney? = try {
		EFVisJourney.valueOf(value)
	} catch (e: Exception) {
		null
	}
}

enum class EFVisJourney(val label: String) {
	`~empty~`("Oui"),
	no("Non"),
}

interface OFVisJourney : IMaterial {
	val f_isJourney: FVisJourney?
}
