package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVsubmitLink(override val parent: Dba_formM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:submitLink", parent, node, value)

interface RFVsubmitLink : IMaterial {
	val f_submitLink: FVsubmitLink
}
