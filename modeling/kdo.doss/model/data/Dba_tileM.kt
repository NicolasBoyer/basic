package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_tileM(override val parent: Ptile, node: Element?) : Material(parent, node), IDataform, RFOtitle, OFRimage {
	override val tag: String get() = "ba:tileM"
	override lateinit var f_title: FOtitle
	override var f_image: FRimage? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_title)?.let { return it }
		f_image?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_title)
		f_image?.let {cb(it)}
	}
}
