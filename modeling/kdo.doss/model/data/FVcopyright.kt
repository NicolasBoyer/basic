package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVcopyright(override val parent: Dba_siteM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:copyright", parent, node, value)

interface OFVcopyright : IMaterial {
	val f_copyright: FVcopyright?
}
