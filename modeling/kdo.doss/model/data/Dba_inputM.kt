package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_inputM(override val parent: Pinput, node: Element?) : Material(parent, node), IDataform, RFVlabel, OFVtype, OFVrequired {
	override val tag: String get() = "ba:inputM"
	override lateinit var f_label: FVlabel
	override var f_type: FVtype? = null
	override var f_required: FVrequired? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_label)?.let { return it }
		f_type?.let { cb(it)?.let { return it } }
		f_required?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_label)
		f_type?.let {cb(it)}
		f_required?.let {cb(it)}
	}
}
