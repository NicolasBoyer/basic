package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVfrag(override val parent: Dba_mwUnM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:frag", parent, node, value)

interface RFVfrag : IMaterial {
	val f_frag: FVfrag
}
