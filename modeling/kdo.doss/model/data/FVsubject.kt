package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVsubject(override val parent: Dba_formM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:subject", parent, node, value)

interface RFVsubject : IMaterial {
	val f_subject: FVsubject
}
