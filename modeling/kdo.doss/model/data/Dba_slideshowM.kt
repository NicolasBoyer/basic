package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_slideshowM(override val parent: Cba_slideshow, node: Element?) : Material(parent, node), IDataform, OFOtitle {
	override val tag: String get() = "ba:slideshowM"
	override var f_title: FOtitle? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		f_title?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		f_title?.let {cb(it)}
	}
}
