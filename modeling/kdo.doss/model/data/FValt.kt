package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FValt(override val parent: Dba_imageM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:alt", parent, node, value)

interface OFValt : IMaterial {
	val f_alt: FValt?
}
