package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVisMag(override val parent: IMaterial /* Dba_imagesM | Dba_resM */, node: Element?, value: String = defaultVal(node)): FieldValue("sp:isMag", parent, node, value) {
	val option: EFVisMag? = try {
		EFVisMag.valueOf(value)
	} catch (e: Exception) {
		null
	}
}

enum class EFVisMag(val label: String) {
	`~empty~`("Oui"),
	no("Non"),
}

interface OFVisMag : IMaterial {
	val f_isMag: FVisMag?
}
