package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FOdesc(override val parent: FGaccessibility, node: Element?): FieldOther<Tba_txt>("sp:desc", parent, node)

interface OFOdesc : IMaterial {
	val f_desc: FOdesc?
}
