package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVisThumbs(override val parent: Dba_imagesM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:isThumbs", parent, node, value) {
	val option: EFVisThumbs? = try {
		EFVisThumbs.valueOf(value)
	} catch (e: Exception) {
		null
	}
}

enum class EFVisThumbs(val label: String) {
	`~empty~`("Oui"),
	no("Non"),
}

interface OFVisThumbs : IMaterial {
	val f_isThumbs: FVisThumbs?
}
