package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVautoScroll(override val parent: Dba_sliderM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:autoScroll", parent, node, value)

interface RFVautoScroll : IMaterial {
	val f_autoScroll: FVautoScroll
}
