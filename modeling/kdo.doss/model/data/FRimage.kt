package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FRimage(override val parent: Dba_tileM, node: Element?): FieldRefItem<Bpng_gif_jpg_jpeg>("sp:image", parent, node, allowedFRimage)

private val allowedFRimage = arrayOf<Class<out IMaterial>>(Bpng_gif_jpg_jpeg::class.java)

interface OFRimage : IMaterial {
	val f_image: FRimage?
}
