package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_resM(override val parent: BExt_img, node: Element?) : MaterialLocalXmlId(parent, node), IDataform, OFOtitle, OFVisMag {
	override val tag: String get() = "ba:resM"
	override var f_title: FOtitle? = null
	override var f_isMag: FVisMag? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		f_title?.let { cb(it)?.let { return it } }
		f_isMag?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		f_title?.let {cb(it)}
		f_isMag?.let {cb(it)}
	}
}
