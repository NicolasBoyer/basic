package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FGaccessibility(override val parent: Dba_videoM, node: Element?): FieldGroup("sp:accessibility", parent, node), OFOdesc {
	override var f_desc: FOdesc? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		f_desc?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		f_desc?.let {cb(it)}
	}
}

interface OFGaccessibility : IMaterial {
	val f_accessibility: FGaccessibility?
}
