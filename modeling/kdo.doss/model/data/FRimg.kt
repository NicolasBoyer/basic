package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FRimg(override val parent: Dba_editoGalleryM, node: Element?): FieldRefItem<Bpng_gif_jpg_jpeg>("sp:img", parent, node, allowedFRimg)

private val allowedFRimg = arrayOf<Class<out IMaterial>>(Bpng_gif_jpg_jpeg::class.java)

interface RFRimg : IMaterial {
	val f_img: FRimg
}
