package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVurl(override val parent: IMaterial /* Dba_url | FGsocialeNetwork */, node: Element?, value: String = defaultVal(node)): FieldValue("sp:url", parent, node, value)

interface RFVurl : IMaterial {
	val f_url: FVurl
}
