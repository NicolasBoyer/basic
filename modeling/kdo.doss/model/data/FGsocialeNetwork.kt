package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FGsocialeNetwork(override val parent: FGsocialNetworks, node: Element?): FieldGroup("sp:socialeNetwork", parent, node), RFVtitle, RFVurl {
	override lateinit var f_title: FVtitle
	override lateinit var f_url: FVurl

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_title)?.let { return it }
		cb(f_url)?.let { return it }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_title)
		cb(f_url)
	}
}

interface MRFGsocialeNetwork : IMaterial {
	val f_socialeNetwork: MultiField<FGsocialeNetwork>
}
