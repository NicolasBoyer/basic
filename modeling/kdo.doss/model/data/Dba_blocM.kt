package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_blocM(override val parent: IMaterial /* Pemphasis | Pinfo | Pvisavis */, node: Element?) : Material(parent, node), IDataform, OFOtitle {
	override val tag: String get() = "ba:blocM"
	override var f_title: FOtitle? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		f_title?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		f_title?.let {cb(it)}
	}
}
