package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_sectionM(override val parent: INone, node: Element?) : Material(parent, node), IDataform, RFVtitle, RFVcoordinates {
	override val tag: String get() = "ba:sectionM"
	override lateinit var f_title: FVtitle
	override lateinit var f_coordinates: FVcoordinates

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_title)?.let { return it }
		cb(f_coordinates)?.let { return it }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_title)
		cb(f_coordinates)
	}
}
