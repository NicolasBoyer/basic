package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVclasses(override val parent: Dba_pageM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:classes", parent, node, value)

interface OFVclasses : IMaterial {
	val f_classes: FVclasses?
}
