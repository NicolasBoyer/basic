package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_editoGalleryM(override val parent: Cba_editoGallery, node: Element?) : Material(parent, node), IDataform, RFRimg {
	override val tag: String get() = "ba:editoGalleryM"
	override lateinit var f_img: FRimg

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_img)?.let { return it }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_img)
	}
}
