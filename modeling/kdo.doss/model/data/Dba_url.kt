package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_url(override val parent: LPH_url, node: Element?) : Material(parent, node), IDataform, RFVurl, OFVtitle {
	override val tag: String get() = "ba:url"
	override lateinit var f_url: FVurl
	override var f_title: FVtitle? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_url)?.let { return it }
		f_title?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_url)
		f_title?.let {cb(it)}
	}
}
