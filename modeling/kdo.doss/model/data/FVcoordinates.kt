package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVcoordinates(override val parent: IMaterial /* Dba_imageM | Dba_sectionM */, node: Element?, value: String = defaultVal(node)): FieldValue("sp:coordinates", parent, node, value)

interface OFVcoordinates : IMaterial {
	val f_coordinates: FVcoordinates?
}
interface RFVcoordinates : OFVcoordinates {
	override val f_coordinates: FVcoordinates
}
