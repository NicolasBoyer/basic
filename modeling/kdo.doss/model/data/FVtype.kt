package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVtype(override val parent: Dba_inputM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:type", parent, node, value) {
	val option: EFVtype? = try {
		EFVtype.valueOf(value)
	} catch (e: Exception) {
		null
	}
}

enum class EFVtype(val label: String) {
	`~empty~`("Texte sur une ligne"),
	textarea("Texte sur plusieurs lignes"),
}

interface OFVtype : IMaterial {
	val f_type: FVtype?
}
