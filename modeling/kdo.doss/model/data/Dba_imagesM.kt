package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_imagesM(override val parent: IMaterial /* Pimages | PmapGallery */, node: Element?) : Material(parent, node), IDataform, OFVisThumbs, OFVisMag, OFVisJourney {
	override val tag: String get() = "ba:imagesM"
	override var f_isThumbs: FVisThumbs? = null
	override var f_isMag: FVisMag? = null
	override var f_isJourney: FVisJourney? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		f_isThumbs?.let { cb(it)?.let { return it } }
		f_isMag?.let { cb(it)?.let { return it } }
		f_isJourney?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		f_isThumbs?.let {cb(it)}
		f_isMag?.let {cb(it)}
		f_isJourney?.let {cb(it)}
	}
}
