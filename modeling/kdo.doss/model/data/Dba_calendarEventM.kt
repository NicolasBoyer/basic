package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_calendarEventM(override val parent: Pevent, node: Element?) : Material(parent, node), IDataform, RFVtitle, RFVlocation, RFVdate, OFVlink {
	override val tag: String get() = "ba:calendarEventM"
	override lateinit var f_title: FVtitle
	override lateinit var f_location: FVlocation
	override lateinit var f_date: FVdate
	override var f_link: FVlink? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_title)?.let { return it }
		cb(f_location)?.let { return it }
		cb(f_date)?.let { return it }
		f_link?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_title)
		cb(f_location)
		cb(f_date)
		f_link?.let {cb(it)}
	}
}
