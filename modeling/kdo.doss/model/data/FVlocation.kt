package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVlocation(override val parent: Dba_calendarEventM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:location", parent, node, value)

interface RFVlocation : IMaterial {
	val f_location: FVlocation
}
