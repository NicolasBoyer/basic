package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_mwUnM(override val parent: Punknown, node: Element?) : Material(parent, node), IDataform, RFVfrag {
	override val tag: String get() = "ba:mwUnM"
	override lateinit var f_frag: FVfrag

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_frag)?.let { return it }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_frag)
	}
}
