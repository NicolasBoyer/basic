package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_siteM(override val parent: Cba_site, node: Element?) : Material(parent, node), IDataform, RFVtitle, OFRlogo, OFVcopyright, OFGsocialNetworks {
	override val tag: String get() = "ba:siteM"
	override lateinit var f_title: FVtitle
	override var f_logo: FRlogo? = null
	override var f_copyright: FVcopyright? = null
	override var f_socialNetworks: FGsocialNetworks? = null

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_title)?.let { return it }
		f_logo?.let { cb(it)?.let { return it } }
		f_copyright?.let { cb(it)?.let { return it } }
		f_socialNetworks?.let { cb(it)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_title)
		f_logo?.let {cb(it)}
		f_copyright?.let {cb(it)}
		f_socialNetworks?.let {cb(it)}
	}
}
