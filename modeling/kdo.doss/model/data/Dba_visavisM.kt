package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_visavisM(override val parent: Cba_visavis, node: Element?) : Material(parent, node), IDataform, RFVratio {
	override val tag: String get() = "ba:visavisM"
	override lateinit var f_ratio: FVratio

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_ratio)?.let { return it }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_ratio)
	}
}
