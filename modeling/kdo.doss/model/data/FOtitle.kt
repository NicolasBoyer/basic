package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FOtitle(override val parent: IMaterial /* Dba_blocM | Dba_calendarM | Dba_formM | Dba_imagesGalleryM | Dba_mapGalleryM | Dba_mediaWebM | Dba_pageM | Dba_resM | Dba_slideshowM | Dba_textsM | Dba_tileM | Dba_tilesM | Dba_videoM | Dba_videosGalleryM */, node: Element?): FieldOther<Tba_title>("sp:title", parent, node)

interface OFOtitle : IMaterial {
	val f_title: FOtitle?
}
interface RFOtitle : OFOtitle {
	override val f_title: FOtitle
}
