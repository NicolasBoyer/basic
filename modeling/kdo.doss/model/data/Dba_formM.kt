package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_formM(override val parent: Cba_form, node: Element?) : Material(parent, node), IDataform, OFOtitle, RFVsubject, RFVsubmitLink {
	override val tag: String get() = "ba:formM"
	override var f_title: FOtitle? = null
	override lateinit var f_subject: FVsubject
	override lateinit var f_submitLink: FVsubmitLink

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		f_title?.let { cb(it)?.let { return it } }
		cb(f_subject)?.let { return it }
		cb(f_submitLink)?.let { return it }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		f_title?.let {cb(it)}
		cb(f_subject)
		cb(f_submitLink)
	}
}
