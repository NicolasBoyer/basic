package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVrequired(override val parent: Dba_inputM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:required", parent, node, value) {
	val option: EFVrequired? = try {
		EFVrequired.valueOf(value)
	} catch (e: Exception) {
		null
	}
}

enum class EFVrequired(val label: String) {
	`~empty~`("Non"),
	yes("Oui"),
}

interface OFVrequired : IMaterial {
	val f_required: FVrequired?
}
