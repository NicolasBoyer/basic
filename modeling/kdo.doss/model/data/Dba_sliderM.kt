package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class Dba_sliderM(override val parent: Pslider, node: Element?) : Material(parent, node), IDataform, RFVautoScroll {
	override val tag: String get() = "ba:sliderM"
	override lateinit var f_autoScroll: FVautoScroll

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		cb(f_autoScroll)?.let { return it }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		cb(f_autoScroll)
	}
}
