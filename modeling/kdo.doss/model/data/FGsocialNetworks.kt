package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FGsocialNetworks(override val parent: Dba_siteM, node: Element?): FieldGroup("sp:socialNetworks", parent, node), MRFGsocialeNetwork {
	override val f_socialeNetwork = MultiField<FGsocialeNetwork>()

	override fun <T> inChildren(cb: (s: IMaterial) -> T?): T? {
		f_socialeNetwork.forEach { p -> cb(p)?.let { return it } }
		return null
	}

	override fun ofChildren(cb: (s: IMaterial) -> Unit) {
		f_socialeNetwork.forEach(cb)
	}
}

interface OFGsocialNetworks : IMaterial {
	val f_socialNetworks: FGsocialNetworks?
}
