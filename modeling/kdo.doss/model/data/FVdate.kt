package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVdate(override val parent: IMaterial /* Dba_calendarEventM | Dba_imageM */, node: Element?, value: String = defaultVal(node)): FieldValue("sp:date", parent, node, value)

interface OFVdate : IMaterial {
	val f_date: FVdate?
}
interface RFVdate : OFVdate {
	override val f_date: FVdate
}
