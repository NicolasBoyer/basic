package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVratio(override val parent: Dba_visavisM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:ratio", parent, node, value) {
	val option: EFVratio? = try {
		EFVratio.valueOf(value)
	} catch (e: Exception) {
		null
	}
}

enum class EFVratio(val label: String) {
	auto("Adapté au contenu"),
	`half-half`("1/2 - 1/2"),
	`one-three`("1/4 - 3/4"),
	`one-two`("1/3 - 2/3"),
	`three-one`("3/4 - 1/4"),
	`two-one`("2/3 - 1/3"),
}

interface RFVratio : IMaterial {
	val f_ratio: FVratio
}
