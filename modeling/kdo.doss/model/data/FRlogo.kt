package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FRlogo(override val parent: Dba_siteM, node: Element?): FieldRefItem<Bpng_gif_jpg_jpeg>("sp:logo", parent, node, allowedFRlogo)

private val allowedFRlogo = arrayOf<Class<out IMaterial>>(Bpng_gif_jpg_jpeg::class.java)

interface OFRlogo : IMaterial {
	val f_logo: FRlogo?
}
