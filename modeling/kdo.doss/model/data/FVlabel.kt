package model.data
import eu.scenari.kdo.material.*
import model.binary.*
import model.compo.*
import model.text.*
import org.w3c.dom.Element

class FVlabel(override val parent: Dba_inputM, node: Element?, value: String = defaultVal(node)): FieldValue("sp:label", parent, node, value)

interface RFVlabel : IMaterial {
	val f_label: FVlabel
}
