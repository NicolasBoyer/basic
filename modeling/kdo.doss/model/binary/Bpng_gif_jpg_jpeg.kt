package model.binary
import eu.scenari.kdo.material.*
import model.compo.*
import model.data.*
import model.text.*
import org.w3c.dom.Element
import eu.scenari.kdo.doer.DoCtx

class Bpng_gif_jpg_jpeg(parent: BinaryRoot) : BinarySpatialWithMeta<Dba_imageM>(parent){
	override val tag: String get() = "sfile:png_gif_jpg_jpeg"
	override fun meta(c: DoCtx): Dba_imageM? = fetchMeta(c, Dba_imageM::class.java)
}
