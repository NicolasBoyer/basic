package model.binary
import eu.scenari.kdo.material.*
import model.compo.*
import model.data.*
import model.text.*
import org.w3c.dom.Element
import eu.scenari.kdo.doer.DoCtx

class Bmp4_f4v_ogv_webm(parent: BinaryRoot) : BinaryWithMeta<Dba_videoM>(parent){
	override val tag: String get() = "sfile:mp4_f4v_ogv_webm"
	override fun meta(c: DoCtx): Dba_videoM? = fetchMeta(c, Dba_videoM::class.java)
}
