import com.scenari.src.ISrcNode
import com.scenari.src.feature.cachedobjects.SrcFeatureCachedObjects
import eu.scenari.kdo.material.*
import eu.scenari.commons.util.xml.*
import eu.scenari.kdo.util.*
import model.binary.*
import model.compo.*
import model.data.*
import model.text.*
import org.w3c.dom.*

fun fromSrcNode(src: ISrcNode): IMaterialRoot? {
	val doc = SrcFeatureCachedObjects.getDom(src, false)
	if (doc == null) {
		return null
	} else {
		val root: Element = doc.documentElement ?: return null
		if (root.nodeName === "sc:item") {
			val n = root.firstElt ?: return null
			val p = MaterialRoot(src, root)
			val nm = n.nodeName
			p.start = when {
				nm === "ba:calendar" -> toCba_calendar(p, n)
				nm === "ba:editoGallery" -> toCba_editoGallery(p, n)
				nm === "ba:form" -> toCba_form(p, n)
				nm === "ba:imagesGallery" -> toCba_imagesGallery(p, n)
				nm === "ba:mapGallery" -> toCba_mapGallery(p, n)
				nm === "ba:mediaWeb" -> toCba_mediaWeb(p, n)
				nm === "ba:page" -> toCba_page(p, n)
				nm === "ba:site" -> toCba_site(p, n)
				nm === "ba:slideshow" -> toCba_slideshow(p, n)
				nm === "ba:texts" -> toCba_texts(p, n)
				nm === "ba:tiles" -> toCba_tiles(p, n)
				nm === "ba:videosGallery" -> toCba_videosGallery(p, n)
				nm === "ba:visavis" -> toCba_visavis(p, n)
				else -> null
			}
			return p
		} else if (root.namespaceURI == "http://www.utc.fr/ics/scenari/v3/file") {
			val p = BinaryRoot(src)
			val nm = root.nodeName
			p.start = when {
				nm === "sfile:mp4_f4v_ogv_webm" -> Bmp4_f4v_ogv_webm(p)
				nm === "sfile:png_gif_jpg_jpeg" -> Bpng_gif_jpg_jpeg(p)
				else -> null
			}
			return p
		} else if (root.namespaceURI == "http://www.utc.fr/ics/scenari/v3/filemeta") {
			val n = root.firstElt ?: return null
			val p = MaterialRoot(src, root)
			val nm = n.nodeName
			p.start = when {
				nm === "ba:imageM" -> toDba_imageM(p, n)
				nm === "ba:videoM" -> toDba_videoM(p, n)
				else -> null
			}
			return p
		} else if (root.nodeName === "imgProps") {
			return SpatialProps(
				(root.getAttribute("standardCharSize") as String?)?.toFloatOrNull(),
				(root.getAttribute("limitCharSize") as String?)?.toFloatOrNull(),
				(root.getAttribute("rotateForbidden") as String?)?.toBooleanStrictOrNull(),
				src
			)
		}
		return null
	}
}

private fun toCba_calendar(p: IMaterial, n: Element?): Cba_calendar {
	val r = Cba_calendar(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:calendarM" && r.meta == null) r.meta = toDba_calendarM(r, ch)
		if (nm === "sp:event") r.s_event.addPart(toPevent(r, ch))
	}
	return r
}

private fun toCba_editoGallery(p: IMaterial, n: Element?): Cba_editoGallery {
	val r = Cba_editoGallery(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:editoGalleryM" && r.meta == null) r.meta = toDba_editoGalleryM(r, ch)
		if (nm === "sp:zone") r.s_zones.addPart(toPzone(r, ch))
	}
	return r
}

private fun toCba_form(p: IMaterial, n: Element?): Cba_form {
	val r = Cba_form(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:formM" && r.meta == null) r.meta = toDba_formM(r, ch)
		if (nm === "sp:input") r.s_input.addPart(toPinput(r, ch))
	}
	return r
}

private fun toCba_imagesGallery(p: IMaterial, n: Element?): Cba_imagesGallery {
	val r = Cba_imagesGallery(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:imagesGalleryM" && r.meta == null) r.meta = toDba_imagesGalleryM(r, ch)
		if (nm === "sp:image") r.s_images.addPart(toPimage(r, ch))
	}
	return r
}

private fun toCba_mapGallery(p: IMaterial, n: Element?): Cba_mapGallery {
	val r = Cba_mapGallery(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:mapGalleryM" && r.meta == null) r.meta = toDba_mapGalleryM(r, ch)
		if (nm === "sp:gallery" && r.p_gallery == null) r.p_gallery = toPgallery(r, ch)
		if (nm === "sp:editoGallery" && r.p_editoGallery == null) r.p_editoGallery = toPeditoGallery(r, ch)
	}
	return r
}

private fun toCba_mediaWeb(p: IMaterial, n: Element?): Cba_mediaWeb {
	val r = Cba_mediaWeb(p, n)
	var tmp_unknown: Punknown? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:mediaWebM" && r.meta == null) r.meta = toDba_mediaWebM(r, ch)
		if (nm === "sp:unknown" && tmp_unknown == null) tmp_unknown = toPunknown(r, ch)
	}
	r.p_unknown = tmp_unknown ?: toPunknown(r, null)
	return r
}

private fun toCba_page(p: IMaterial, n: Element?): Cba_page {
	val r = Cba_page(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:pageM" && r.meta == null) r.meta = toDba_pageM(r, ch)
		if (nm === "sp:slider" && r.p_slider == null) r.p_slider = toPslider(r, ch)
		if (nm === "sp:members") r.s_1.addPart(toPmembers(r, ch))
		if (nm === "sp:calendar") r.s_1.addPart(toPcalendar(r, ch))
		if (nm === "sp:images") r.s_1.addPart(toPimages(r, ch))
		if (nm === "sp:mapGallery") r.s_1.addPart(toPmapGallery(r, ch))
		if (nm === "sp:videos") r.s_1.addPart(toPvideos(r, ch))
		if (nm === "sp:texts") r.s_1.addPart(toPtexts(r, ch))
		if (nm === "sp:form") r.s_1.addPart(toPform(r, ch))
		if (nm === "sp:tiles") r.s_1.addPart(toPtiles(r, ch))
	}
	return r
}

private fun toCba_site(p: IMaterial, n: Element?): Cba_site {
	val r = Cba_site(p as IMaterialRoot, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:siteM" && r.meta == null) r.meta = toDba_siteM(r, ch)
		if (nm === "sp:home" && r.p_home == null) r.p_home = toPhome(r, ch)
		if (nm === "sp:page") r.s_page.addPart(toPpage(r, ch))
	}
	return r
}

private fun toCba_slideshow(p: IMaterial, n: Element?): Cba_slideshow {
	val r = Cba_slideshow(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:slideshowM" && r.meta == null) r.meta = toDba_slideshowM(r, ch)
		if (nm === "sp:image") r.s_slides.addPart(toPimage(r, ch))
		if (nm === "sp:text") r.s_slides.addPart(toPtext(r, ch))
	}
	return r
}

private fun toCba_texts(p: IMaterial, n: Element?): Cba_texts {
	val r = Cba_texts(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:textsM" && r.meta == null) r.meta = toDba_textsM(r, ch)
		if (nm === "sp:info") {
			(r.s_block ?: CompSet<IPart /* Pemphasis | Pinfo | Pvisavis */>().also { r.s_block = it }).addPart(toPinfo(r, ch))
		}
		if (nm === "sp:visavis") {
			(r.s_block ?: CompSet<IPart /* Pemphasis | Pinfo | Pvisavis */>().also { r.s_block = it }).addPart(toPvisavis(r, ch))
		}
		if (nm === "sp:emphasis") {
			(r.s_block ?: CompSet<IPart /* Pemphasis | Pinfo | Pvisavis */>().also { r.s_block = it }).addPart(toPemphasis(r, ch))
		}
	}
	return r
}

private fun toCba_tiles(p: IMaterial, n: Element?): Cba_tiles {
	val r = Cba_tiles(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:tilesM" && r.meta == null) r.meta = toDba_tilesM(r, ch)
		if (nm === "sp:tile") r.s_tile.addPart(toPtile(r, ch))
	}
	return r
}

private fun toCba_videosGallery(p: IMaterial, n: Element?): Cba_videosGallery {
	val r = Cba_videosGallery(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:videosGalleryM" && r.meta == null) r.meta = toDba_videosGalleryM(r, ch)
		if (nm === "sp:video") r.s_videos.addPart(toPvideo(r, ch))
	}
	return r
}

private fun toCba_visavis(p: IMaterial, n: Element?): Cba_visavis {
	val r = Cba_visavis(p, n)
	var tmp_first: Pfirst? = null
	var tmp_next: Pnext? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:visavisM" && r.meta == null) r.meta = toDba_visavisM(r, ch)
		if (nm === "sp:first" && tmp_first == null) tmp_first = toPfirst(r, ch)
		if (nm === "sp:next" && tmp_next == null) tmp_next = toPnext(r, ch)
	}
	r.p_first = tmp_first ?: toPfirst(r, null)
	r.p_next = tmp_next ?: toPnext(r, null)
	return r
}

private fun toDba_blocM(p: IMaterial, n: Element?): Dba_blocM {
	val r = Dba_blocM(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
	}
	return r
}

private fun toDba_calendarEventM(p: IMaterial, n: Element?): Dba_calendarEventM {
	val r = Dba_calendarEventM(p as Pevent, n)
	var tmp_title: FVtitle? = null
	var tmp_location: FVlocation? = null
	var tmp_date: FVdate? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && tmp_title == null) tmp_title = FVtitle(r, ch)
		if (nm === "sp:location" && tmp_location == null) tmp_location = FVlocation(r as Dba_calendarEventM, ch)
		if (nm === "sp:date" && tmp_date == null) tmp_date = FVdate(r, ch)
		if (nm === "sp:link" && r.f_link == null) r.f_link = FVlink(r as Dba_calendarEventM, ch)
	}
	r.f_title = tmp_title ?: FVtitle(r, null)
	r.f_location = tmp_location ?: FVlocation(r as Dba_calendarEventM, null)
	r.f_date = tmp_date ?: FVdate(r, null)
	return r
}

private fun toDba_calendarM(p: IMaterial, n: Element?): Dba_calendarM {
	val r = Dba_calendarM(p as Cba_calendar, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
	}
	return r
}

private fun toDba_editoGalleryM(p: IMaterial, n: Element?): Dba_editoGalleryM {
	val r = Dba_editoGalleryM(p as Cba_editoGallery, n)
	var tmp_img: FRimg? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:img" && tmp_img == null) tmp_img = FRimg(r as Dba_editoGalleryM, ch)
	}
	r.f_img = tmp_img ?: FRimg(r as Dba_editoGalleryM, null)
	return r
}

private fun toDba_formM(p: IMaterial, n: Element?): Dba_formM {
	val r = Dba_formM(p as Cba_form, n)
	var tmp_subject: FVsubject? = null
	var tmp_submitLink: FVsubmitLink? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
		if (nm === "sp:subject" && tmp_subject == null) tmp_subject = FVsubject(r as Dba_formM, ch)
		if (nm === "sp:submitLink" && tmp_submitLink == null) tmp_submitLink = FVsubmitLink(r as Dba_formM, ch)
	}
	r.f_subject = tmp_subject ?: FVsubject(r as Dba_formM, null)
	r.f_submitLink = tmp_submitLink ?: FVsubmitLink(r as Dba_formM, null)
	return r
}

private fun toDba_imageM(p: IMaterial, n: Element?): Dba_imageM {
	val r = Dba_imageM(p as MaterialRoot, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = FVtitle(r, ch)
		if (nm === "sp:alt" && r.f_alt == null) r.f_alt = FValt(r as Dba_imageM, ch)
		if (nm === "sp:coordinates" && r.f_coordinates == null) r.f_coordinates = FVcoordinates(r, ch)
		if (nm === "sp:date" && r.f_date == null) r.f_date = FVdate(r, ch)
	}
	return r
}

private fun toDba_imagesGalleryM(p: IMaterial, n: Element?): Dba_imagesGalleryM {
	val r = Dba_imagesGalleryM(p as Cba_imagesGallery, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
	}
	return r
}

private fun toDba_imagesM(p: IMaterial, n: Element?): Dba_imagesM {
	val r = Dba_imagesM(p, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:isThumbs" && r.f_isThumbs == null) r.f_isThumbs = FVisThumbs(r as Dba_imagesM, ch)
		if (nm === "sp:isMag" && r.f_isMag == null) r.f_isMag = FVisMag(r, ch)
		if (nm === "sp:isJourney" && r.f_isJourney == null) r.f_isJourney = FVisJourney(r as Dba_imagesM, ch)
	}
	return r
}

private fun toDba_inputM(p: IMaterial, n: Element?): Dba_inputM {
	val r = Dba_inputM(p as Pinput, n)
	var tmp_label: FVlabel? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:label" && tmp_label == null) tmp_label = FVlabel(r as Dba_inputM, ch)
		if (nm === "sp:type" && r.f_type == null) r.f_type = FVtype(r as Dba_inputM, ch)
		if (nm === "sp:required" && r.f_required == null) r.f_required = FVrequired(r as Dba_inputM, ch)
	}
	r.f_label = tmp_label ?: FVlabel(r as Dba_inputM, null)
	return r
}

private fun toDba_mapGalleryM(p: IMaterial, n: Element?): Dba_mapGalleryM {
	val r = Dba_mapGalleryM(p as Cba_mapGallery, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
	}
	return r
}

private fun toDba_mediaWebM(p: IMaterial, n: Element?): Dba_mediaWebM {
	val r = Dba_mediaWebM(p as Cba_mediaWeb, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
	}
	return r
}

private fun toDba_mwUnM(p: IMaterial, n: Element?): Dba_mwUnM {
	val r = Dba_mwUnM(p as Punknown, n)
	var tmp_frag: FVfrag? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:frag" && tmp_frag == null) tmp_frag = FVfrag(r as Dba_mwUnM, ch)
	}
	r.f_frag = tmp_frag ?: FVfrag(r as Dba_mwUnM, null)
	return r
}

private fun toDba_pageM(p: IMaterial, n: Element?): Dba_pageM {
	val r = Dba_pageM(p as Cba_page, n)
	var tmp_title: FOtitle? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && tmp_title == null) tmp_title = toFOtitle(r, ch)
		if (nm === "sp:classes" && r.f_classes == null) r.f_classes = FVclasses(r as Dba_pageM, ch)
	}
	r.f_title = tmp_title ?: toFOtitle(r, null)
	return r
}

private fun toDba_resM(p: IMaterial, n: Element?): Dba_resM {
	val r = Dba_resM(p as BExt_img, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
		if (nm === "sp:isMag" && r.f_isMag == null) r.f_isMag = FVisMag(r, ch)
	}
	return r
}

private fun toDba_siteM(p: IMaterial, n: Element?): Dba_siteM {
	val r = Dba_siteM(p as Cba_site, n)
	var tmp_title: FVtitle? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && tmp_title == null) tmp_title = FVtitle(r, ch)
		if (nm === "sp:logo" && r.f_logo == null) r.f_logo = FRlogo(r as Dba_siteM, ch)
		if (nm === "sp:copyright" && r.f_copyright == null) r.f_copyright = FVcopyright(r as Dba_siteM, ch)
		if (nm === "sp:socialNetworks" && r.f_socialNetworks == null) r.f_socialNetworks = toFGsocialNetworks(r, ch)
	}
	r.f_title = tmp_title ?: FVtitle(r, null)
	return r
}

private fun toDba_sliderM(p: IMaterial, n: Element?): Dba_sliderM {
	val r = Dba_sliderM(p as Pslider, n)
	var tmp_autoScroll: FVautoScroll? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:autoScroll" && tmp_autoScroll == null) tmp_autoScroll = FVautoScroll(r as Dba_sliderM, ch)
	}
	r.f_autoScroll = tmp_autoScroll ?: FVautoScroll(r as Dba_sliderM, null)
	return r
}

private fun toDba_slideshowM(p: IMaterial, n: Element?): Dba_slideshowM {
	val r = Dba_slideshowM(p as Cba_slideshow, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
	}
	return r
}

private fun toDba_textsM(p: IMaterial, n: Element?): Dba_textsM {
	val r = Dba_textsM(p as Cba_texts, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
	}
	return r
}

private fun toDba_tileM(p: IMaterial, n: Element?): Dba_tileM {
	val r = Dba_tileM(p as Ptile, n)
	var tmp_title: FOtitle? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && tmp_title == null) tmp_title = toFOtitle(r, ch)
		if (nm === "sp:image" && r.f_image == null) r.f_image = FRimage(r as Dba_tileM, ch)
	}
	r.f_title = tmp_title ?: toFOtitle(r, null)
	return r
}

private fun toDba_tilesM(p: IMaterial, n: Element?): Dba_tilesM {
	val r = Dba_tilesM(p as Cba_tiles, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
	}
	return r
}

private fun toDba_url(p: IMaterial, n: Element?): Dba_url {
	val r = Dba_url(p as LPH_url, n)
	var tmp_url: FVurl? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:url" && tmp_url == null) tmp_url = FVurl(r, ch)
		if (nm === "sp:title" && r.f_title == null) r.f_title = FVtitle(r, ch)
	}
	r.f_url = tmp_url ?: FVurl(r, null)
	return r
}

private fun toDba_videoM(p: IMaterial, n: Element?): Dba_videoM {
	val r = Dba_videoM(p as MaterialRoot, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
		if (nm === "sp:accessibility" && r.f_accessibility == null) r.f_accessibility = toFGaccessibility(r, ch)
	}
	return r
}

private fun toDba_videosGalleryM(p: IMaterial, n: Element?): Dba_videosGalleryM {
	val r = Dba_videosGalleryM(p as Cba_videosGallery, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && r.f_title == null) r.f_title = toFOtitle(r, ch)
	}
	return r
}

private fun toDba_visavisM(p: IMaterial, n: Element?): Dba_visavisM {
	val r = Dba_visavisM(p as Cba_visavis, n)
	var tmp_ratio: FVratio? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:ratio" && tmp_ratio == null) tmp_ratio = FVratio(r as Dba_visavisM, ch)
	}
	r.f_ratio = tmp_ratio ?: FVratio(r as Dba_visavisM, null)
	return r
}

private fun toTba_title(p: IMaterial, n: Element?): Tba_title {
	val r = Tba_title(p as FOtitle, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "sc:para") r.para = toBPara(r, ch)
	}
	if(!r.isParaInited) r.para = toBPara(r, null)
	return r
}

private fun toTba_txt(p: IMaterial, n: Element?): Tba_txt {
	val r = Tba_txt(p, n)
	n?.ofChildrenElt {ch ->
		r.addTag(toTextBlocks(r, ch))
	}
	return r
}

private fun toPcalendar(p: IMaterial, n: Element?): Pcalendar {
	val r = Pcalendar(p as Cba_page, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:calendar") r.partBody = toCba_calendar(r, ch)
	}
	return r
}

private fun toPeditoGallery(p: IMaterial, n: Element?): PeditoGallery {
	val r = PeditoGallery(p as Cba_mapGallery, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:editoGallery") r.partBody = toCba_editoGallery(r, ch)
	}
	return r
}

private fun toPemphasis(p: IMaterial, n: Element?): Pemphasis {
	val r = Pemphasis(p as Cba_texts, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:blocM") r.meta = toDba_blocM(r, ch)
		else if (nm === "ba:txt") r.partBody = toTba_txt(r, ch)
	}
	return r
}

private fun toPevent(p: IMaterial, n: Element?): Pevent {
	val r = Pevent(p as Cba_calendar, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:calendarEventM") r.meta = toDba_calendarEventM(r, ch)
	}
	return r
}

private fun toPfirst(p: IMaterial, n: Element?): Pfirst {
	val r = Pfirst(p as Cba_visavis, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:txt") r.partBody = toTba_txt(r, ch)
	}
	return r
}

private fun toPform(p: IMaterial, n: Element?): Pform {
	val r = Pform(p as Cba_page, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:form") r.partBody = toCba_form(r, ch)
	}
	return r
}

private fun toPgallery(p: IMaterial, n: Element?): Pgallery {
	val r = Pgallery(p as Cba_mapGallery, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:imagesGallery") r.partBody = toCba_imagesGallery(r, ch)
	}
	return r
}

private fun toPhome(p: IMaterial, n: Element?): Phome {
	return Phome(p as Cba_site, n)
}

private fun toPimage(p: IMaterial, n: Element?): Pimage {
	return Pimage(p as IComposition, n)
}

private fun toPimages(p: IMaterial, n: Element?): Pimages {
	val r = Pimages(p as Cba_page, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:imagesM") r.meta = toDba_imagesM(r, ch)
		else if (nm === "ba:imagesGallery") r.partBody = toCba_imagesGallery(r, ch)
	}
	return r
}

private fun toPinfo(p: IMaterial, n: Element?): Pinfo {
	val r = Pinfo(p as Cba_texts, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:blocM") r.meta = toDba_blocM(r, ch)
		else if (nm === "ba:txt") r.partBody = toTba_txt(r, ch)
	}
	return r
}

private fun toPinput(p: IMaterial, n: Element?): Pinput {
	val r = Pinput(p as Cba_form, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:inputM") r.meta = toDba_inputM(r, ch)
	}
	return r
}

private fun toPmapGallery(p: IMaterial, n: Element?): PmapGallery {
	val r = PmapGallery(p as Cba_page, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:imagesM") r.meta = toDba_imagesM(r, ch)
		else if (nm === "ba:mapGallery") r.partBody = toCba_mapGallery(r, ch)
	}
	return r
}

private fun toPmembers(p: IMaterial, n: Element?): Pmembers {
	val r = Pmembers(p as Cba_page, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:imagesGallery") r.partBody = toCba_imagesGallery(r, ch)
	}
	return r
}

private fun toPnext(p: IMaterial, n: Element?): Pnext {
	val r = Pnext(p as Cba_visavis, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:txt") r.partBody = toTba_txt(r, ch)
	}
	return r
}

private fun toPpage(p: IMaterial, n: Element?): Ppage {
	return Ppage(p as Cba_site, n)
}

private fun toPslider(p: IMaterial, n: Element?): Pslider {
	val r = Pslider(p as Cba_page, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:sliderM") r.meta = toDba_sliderM(r, ch)
	}
	return r
}

private fun toPtext(p: IMaterial, n: Element?): Ptext {
	val r = Ptext(p as Cba_slideshow, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:txt") r.partBody = toTba_txt(r, ch)
	}
	return r
}

private fun toPtexts(p: IMaterial, n: Element?): Ptexts {
	val r = Ptexts(p as Cba_page, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:texts") r.partBody = toCba_texts(r, ch)
	}
	return r
}

private fun toPtile(p: IMaterial, n: Element?): Ptile {
	val r = Ptile(p as Cba_tiles, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:tileM") r.meta = toDba_tileM(r, ch)
	}
	return r
}

private fun toPtiles(p: IMaterial, n: Element?): Ptiles {
	val r = Ptiles(p as Cba_page, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:tiles") r.partBody = toCba_tiles(r, ch)
	}
	return r
}

private fun toPunknown(p: IMaterial, n: Element?): Punknown {
	val r = Punknown(p as Cba_mediaWeb, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:mwUnM") r.meta = toDba_mwUnM(r, ch)
	}
	return r
}

private fun toPvideo(p: IMaterial, n: Element?): Pvideo {
	return Pvideo(p as Cba_videosGallery, n)
}

private fun toPvideos(p: IMaterial, n: Element?): Pvideos {
	val r = Pvideos(p as Cba_page, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:videosGallery") r.partBody = toCba_videosGallery(r, ch)
	}
	return r
}

private fun toPvisavis(p: IMaterial, n: Element?): Pvisavis {
	val r = Pvisavis(p as Cba_texts, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "ba:blocM") r.meta = toDba_blocM(r, ch)
		else if (nm === "ba:visavis") r.partBody = toCba_visavis(r, ch)
	}
	return r
}

private fun toPzone(p: IMaterial, n: Element?): Pzone {
	val r = Pzone(p as Cba_editoGallery, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "ba:slideshow") r.partBody = toCba_slideshow(r, ch)
	}
	return r
}

private fun toFOdesc(p: IMaterial, n: Element?): FOdesc {
	val r = FOdesc(p as FGaccessibility, n)
	r.fieldBody = n?.inChildrenElt {ch ->
		if (ch.nodeName === "ba:txt") toTba_txt(r, ch)
		else null
	} ?: toTba_txt(r, null)
	return r
}

private fun toFOtitle(p: IMaterial, n: Element?): FOtitle {
	val r = FOtitle(p, n)
	r.fieldBody = n?.inChildrenElt {ch ->
		if (ch.nodeName === "ba:title") toTba_title(r, ch)
		else null
	} ?: toTba_title(r, null)
	return r
}

private fun toFGaccessibility(p: IMaterial, n: Element?): FGaccessibility {
	val r = FGaccessibility(p as Dba_videoM, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:desc" && r.f_desc == null) r.f_desc = toFOdesc(r, ch)
	}
	return r
}

private fun toFGsocialNetworks(p: IMaterial, n: Element?): FGsocialNetworks {
	val r = FGsocialNetworks(p as Dba_siteM, n)
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:socialeNetwork") r.f_socialeNetwork.addField(toFGsocialeNetwork(r, ch))
	}
	return r
}

private fun toFGsocialeNetwork(p: IMaterial, n: Element?): FGsocialeNetwork {
	val r = FGsocialeNetwork(p as FGsocialNetworks, n)
	var tmp_title: FVtitle? = null
	var tmp_url: FVurl? = null
	n?.ofChildrenElt {ch ->
		val nm = ch.nodeName
		if (nm === "sp:title" && tmp_title == null) tmp_title = FVtitle(r, ch)
		if (nm === "sp:url" && tmp_url == null) tmp_url = FVurl(r, ch)
	}
	r.f_title = tmp_title ?: FVtitle(r, null)
	r.f_url = tmp_url ?: FVurl(r, null)
	return r
}

private fun toTextBlocks(p: IMaterial, n: Element?): ITextBlock? {
	if (n == null || n.nodeName === "sc:para") return toBPara(p, n)
	val nm = n.nodeName
	val ro = n.getAttribute("role") ?: ""
	if (nm === "sc:itemizedList") {
		if (ro == "") return toBIL_(p, n)
	}
	if (nm === "sc:orderedList") {
		if (ro == "") return toBOL_(p, n)
	}
	if (nm === "sc:table") {
		if (ro == "") return toBTable_(p, n)
	}
	if (nm === "sc:extBlock") {
		if (ro == "img") return toBExt_img(p, n)
	}
	return null
}

private fun toTextInlines(p: IMaterial, n: Node): ITextInline? {
	if (n.nodeType == Node.TEXT_NODE) return TextChars(p, n as Text)
	if (n !is Element) return null
	val nm = n.nodeName
	val ro = n.getAttribute("role") ?: ""
	if (nm === "sc:uLink") {
		if (ro == "page") return toLRef_page(p, n)
	}
	if (nm === "sc:phrase") {
		if (ro == "quote") return toLPH_quote(p, n)
		if (ro == "url") return toLPH_url(p, n)
	}
	if (nm === "sc:inlineStyle") {
		if (ro == "emp") return toLIS_emp(p, n)
		if (ro == "spec") return toLIS_spec(p, n)
		if (ro == "sub") return toLIS_sub(p, n)
		if (ro == "sup") return toLIS_sup(p, n)
	}
	if (nm === "sc:inlineImg") {
		if (ro == "ico") return LImg_ico(p, n)
	}
	return null
}

private fun toBPara(p: IMaterial, n: Element?): BPara {
	val r = BPara(p, n)
	n?.ofChildren {ch ->
		r.addTag(toTextInlines(r, ch))
	}
	return r
}

private fun toBTblCaption(p: IMaterial, n: Element?): BTblCaption {
	val r = BTblCaption(p as BTable_, n)
	n?.ofChildren {ch ->
		r.addTag(toTextInlines(r, ch))
	}
	return r
}

private fun toBLI(p: IMaterial, n: Element?): BLI {
	val r = BLI(p, n)
	n?.ofChildrenElt {ch ->
		r.addTag(toTextBlocks(r, ch))
	}
	return r
}

private fun toBIL_(p: IMaterial, n: Element?): BIL_ {
	val r = BIL_(p, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "sc:listItem") r.addTag(toBLI(r, ch))
	}
	return r
}

private fun toBOL_(p: IMaterial, n: Element?): BOL_ {
	val r = BOL_(p, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "sc:listItem") r.addTag(toBLI(r, ch))
	}
	return r
}

private fun toBTable_(p: IMaterial, n: Element?): BTable_ {
	val r = BTable_(p, n)
	n?.ofChildrenElt {ch ->
		val ro = ch.getAttribute("role") ?: ""
		if (ch.nodeName === "sc:column") {
			if(ro == "") r.addCol(BCol_(r as BTable_, ch))
			else if(ro == "head") r.addCol(BCol_head(r as BTable_, ch))
		} else if (ch.nodeName === "sc:row") {
			if(ro == "") r.addRow(toBTR_(r, ch))
			else if(ro == "head") r.addRow(toBTR_head(r, ch))
		} else if (ch.nodeName === "sc:caption") {
			r.caption = toBTblCaption(r, ch)
		}
	}
	return r
}

private fun toBTR_(p: IMaterial, n: Element?): BTR_ {
	val r = BTR_(p as BTable_, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "sc:cell") {
			val ro = ch.getAttribute("role") ?: ""
			if(ro == "") r.addTag(toBTD_(r, ch))
			else if(ro == "num") r.addTag(toBTD_num(r, ch))
			else if(ro == "word") r.addTag(toBTD_word(r, ch))
		}
	}
	return r
}

private fun toBTR_head(p: IMaterial, n: Element?): BTR_head {
	val r = BTR_head(p as BTable_, n)
	n?.ofChildrenElt {ch ->
		if (ch.nodeName === "sc:cell") {
			val ro = ch.getAttribute("role") ?: ""
			if(ro == "") r.addTag(toBTD_(r, ch))
			else if(ro == "num") r.addTag(toBTD_num(r, ch))
			else if(ro == "word") r.addTag(toBTD_word(r, ch))
		}
	}
	return r
}

private fun toBTD_(p: IMaterial, n: Element?): BTD_ {
	val r = BTD_(p, n)
	n?.ofChildrenElt {ch ->
		r.addTag(toTextBlocks(r, ch))
	}
	return r
}

private fun toBTD_num(p: IMaterial, n: Element?): BTD_num {
	val r = BTD_num(p, n)
	n?.ofChildrenElt {ch ->
		r.addTag(toTextBlocks(r, ch))
	}
	return r
}

private fun toBTD_word(p: IMaterial, n: Element?): BTD_word {
	val r = BTD_word(p, n)
	n?.ofChildrenElt {ch ->
		r.addTag(toTextBlocks(r, ch))
	}
	return r
}

private fun toBExt_img(p: IMaterial, n: Element?): BExt_img {
	val r = BExt_img(p, n)
	n?.ofChildrenElt {ch ->
		if(r.meta == null) {
			if (ch.nodeName === "ba:resM") r.meta = toDba_resM(r, ch)
		}
	}
	return r
}

private fun toLRef_page(p: IMaterial, n: Element?): LRef_page {
	val r = LRef_page(p, n)
	n?.ofChildren {ch ->
		r.addTag(toTextInlines(r, ch))
	}
	return r
}

private fun toLPH_quote(p: IMaterial, n: Element?): LPH_quote {
	val r = LPH_quote(p, n)
	n?.ofChildren {ch ->
		r.addTag(toTextInlines(r, ch))
	}
	return r
}

private fun toLPH_url(p: IMaterial, n: Element?): LPH_url {
	val r = LPH_url(p, n)
	n?.ofChildren {ch ->
		if(r.meta == null) {
			if (ch.nodeName === "ba:url") r.meta = toDba_url(r, ch as Element)
		}
		r.addTag(toTextInlines(r, ch))
	}
	return r
}

private fun toLIS_emp(p: IMaterial, n: Element?): LIS_emp {
	val r = LIS_emp(p, n)
	n?.ofChildren {ch ->
		r.addTag(toTextInlines(r, ch))
	}
	return r
}

private fun toLIS_spec(p: IMaterial, n: Element?): LIS_spec {
	val r = LIS_spec(p, n)
	n?.ofChildren {ch ->
		r.addTag(toTextInlines(r, ch))
	}
	return r
}

private fun toLIS_sub(p: IMaterial, n: Element?): LIS_sub {
	val r = LIS_sub(p, n)
	n?.ofChildren {ch ->
		r.addTag(toTextInlines(r, ch))
	}
	return r
}

private fun toLIS_sup(p: IMaterial, n: Element?): LIS_sup {
	val r = LIS_sup(p, n)
	n?.ofChildren {ch ->
		r.addTag(toTextInlines(r, ch))
	}
	return r
}
