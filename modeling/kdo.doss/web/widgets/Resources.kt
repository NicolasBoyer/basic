package web.widgets

import eu.scenari.kdo.doer.binaries.DoerBinary
import eu.scenari.kdo.doer.binaries.DoerBinaryTransform
import eu.scenari.kdo.material.IBinary
import eu.scenari.src.transform.TfmParams
import model.binary.Bmp4_f4v_ogv_webm
import model.binary.Bpng_gif_jpg_jpeg
import web.BinaryPg

open class Video : BinaryPg<IBinary, Bmp4_f4v_ogv_webm>(
    DoerBinary()
)

open class LargeImg : BinaryPg<IBinary, Bpng_gif_jpg_jpeg>(
    DoerBinaryTransform(TfmParams.ofType("img2img").setParam("sizeRules", "Px(Bounds(maxH'3000'maxW'3000'))"), true)
)

open class MembersImg : BinaryPg<IBinary, Bpng_gif_jpg_jpeg>(
    DoerBinaryTransform(TfmParams.ofType("img2img").setParam("sizeRules", "Px(Bounds(maxH'400'maxW'400'))"), true)
)

open class MediumBlockImg : BinaryPg<IBinary, Bpng_gif_jpg_jpeg>(
    DoerBinaryTransform(TfmParams.ofType("img2img").setParam("sizeRules", "Px(ScSCS(fontPt'10')Bounds(maxH'600'maxW'600'))"), true)
)

open class LogoImg : BinaryPg<IBinary, Bpng_gif_jpg_jpeg>(
    DoerBinaryTransform(TfmParams.ofType("img2img").setParam("sizeRules", "Px(ScSCS(fontPt'10')Bounds(maxH'150'))"), true)
)

open class InlineImg : BinaryPg<IBinary, Bpng_gif_jpg_jpeg>(
    DoerBinaryTransform(TfmParams.ofType("img2img").setParam("sizeRules", "Px(ScSCS(fontPt'10')Bounds(maxH'100'))"), true)
)

open class GalleryImg : BinaryPg<IBinary, Bpng_gif_jpg_jpeg>(
    DoerBinaryTransform(TfmParams.ofType("img2img").setParam("sizeRules", "Px(Bounds(maxH'200'maxW'300'))"), true)
)

open class LargeGalleryImg : BinaryPg<IBinary, Bpng_gif_jpg_jpeg>(
    DoerBinaryTransform(TfmParams.ofType("img2img").setParam("sizeRules", "Px(Bounds(maxH'2000'maxW'2000'))"), true)
)

//
//open class FavIconImg : DoerSvgOrBitmap<IBinary, Bsvg, Bpng_gif_jpg_jpeg>(
//    imgNoRedim
//)

//private val imgNoRedim = DoerBinary<Bpng_gif_jpg_jpeg>()
//private val imgRedim = DoerBinaryTransform<Bpng_gif_jpg_jpeg>(TfmParams.ofType("img2img").setParam("sizeRules", "Px(ScSCS(fontPt'10'))"), true)