package web.widgets

import eu.scenari.kdo.doer.*
import model.compo.*

open class Form : IDoer {
    fun writeForm(f: Cba_form?, o: IDoHtml, c: DoCtx) {
        o.elt("form") {
            f?.s_input ?.forEach { input ->
                writeInput(input, o, c)
            }
        }
    }

    private fun writeInput(i: Pinput, o: IDoHtml, c: DoCtx) {
        o.elt("tr") {
            val label = i.meta?.f_label?.value
            val isRequired = i.meta?.f_required?.value == "yes"
            val type = i.meta?.f_type?.value
            div {
                elt("label") {
                    att("for", label)
                    txt(label + if (isRequired) " *" else "")
                }
                tag(if (type == "textarea") "textarea" else "input", id = label) {
                    att("type", "text")
                    att("name", label)
                    if (isRequired){
                        att("required", "")
                    }
                }
            }
        }
    }
}