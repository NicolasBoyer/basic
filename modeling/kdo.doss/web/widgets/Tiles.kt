package web.widgets

import eu.scenari.kdo.doer.*
import eu.scenari.kdo.material.IBinary
import eu.scenari.kdo.organizers.DoBreadcrumbs
import eu.scenari.kdo.util.cast
import model.compo.*
import model.data.Dba_imageM
import web.PagePg

open class Tiles : IDoer {
    fun writeTiles(t: Cba_tiles, o: IDoHtml, c: DoCtx) {
        t.s_tile.forEach { part ->
            writeTile(part, o, c)
        }
    }

    private fun writeTile(t: Ptile, o: IDoHtml, c: DoCtx) {
        o.tag("a") {
            val bcs = c.doer<DoBreadcrumbs>()
            val pgTarget = t.partBody(c)?.let { bcs.findPg(it, c.doer<PagePg>()) { bc -> bc.size == 1 } ?: bcs.getOrAddChildPage(it, c.doer<PagePg>()) }
            att("href", c.siteMgr.urlToPage(pgTarget))
            val image = t.meta?.f_image
            if (image != null) {
                elt("img") {
                    val target = t.meta?.f_image?.target(c)
                    val meta = target?.meta(c)?.cast<Dba_imageM>()
                    att("alt", meta?.f_alt?.value ?: meta?.f_title?.value)
                    att("data-src", target?.cast<IBinary>()?.let { pImage -> c.siteMgr.urlToPage(c.singletonPage(pImage, c.doer<MediumBlockImg>())) })
                }
            }
            div("title") {
                t.meta?.f_title?.fieldBody.also { c.doer<TiHtml>().writePara(it, o, c) }
            }
        }
    }
}