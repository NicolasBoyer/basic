package web.widgets

import eu.scenari.kdo.doer.*
import eu.scenari.kdo.material.IBinary
import eu.scenari.kdo.util.cast
import model.compo.*
import model.data.Dba_imageM

open class Gallery : IDoer {
    fun writeGallery(ig: Cba_imagesGallery, o: IDoHtml, c: DoCtx) {
        o.div("gallery") {
            ig.s_images.forEach { image ->
                val target = image.target(c)
                val meta = target?.meta(c)?.cast<Dba_imageM>()
                o.tag("a") {
                    att("target", "_blank")
                    att("href", target?.cast<IBinary>()?.let { pImage -> c.siteMgr.urlToPage(c.singletonPage(pImage, c.doer<LargeGalleryImg>())) })
                    att("title", meta?.f_title?.value)
                    elt("img") {
                        att("alt", meta?.f_alt?.value ?: meta?.f_title?.value)
                        att("data-src", target?.cast<IBinary>()?.let { pImage -> c.siteMgr.urlToPage(c.singletonPage(pImage, c.doer<GalleryImg>())) })
                    }
                }
            }
        }
    }
}