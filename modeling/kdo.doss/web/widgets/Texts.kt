package web.widgets

import eu.scenari.kdo.doer.*
import eu.scenari.kdo.material.IPart
import eu.scenari.kdo.material.ITextBlock
import eu.scenari.kdo.material.TextMultiPara
import eu.scenari.kdo.util.cast
import model.compo.*
import model.data.Dba_blocM

open class Texts : IDoer {
    fun writeTexts(t: Cba_texts, o: IDoHtml, c: DoCtx) {
        t.s_block?.forEach { part ->
            writeBlock(part, o, c)
        }
    }

    private fun writeBlock(p: IPart, o: IDoHtml, c: DoCtx) {
        when (p) {
            is Pinfo -> writeArticle(p, p.partBody, o, c)
            is Pemphasis ->  writeArticle(p, p.partBody, o, c, "emphasis")
            is Pvisavis ->  writeVisAvisArticle(p, p.partBody, o, c)
        }
    }

    private fun writeArticle(p: IPart, body: TextMultiPara<ITextBlock>?, o: IDoHtml, c: DoCtx, classes: String = "") {
        o.article("block $classes") {
            p.meta?.cast<Dba_blocM>()?.let { writeBlocTi(it, this, c) }
            c.doer<TxtHtml>().writeText(body, this, c)
        }
    }

    private fun writeVisAvisArticle(p: IPart, v: Cba_visavis?, o: IDoHtml, c: DoCtx) {
        o.article("block visavis ${v?.meta?.f_ratio?.value}") {
            p.meta?.cast<Dba_blocM>()?.let { writeBlocTi(it, this, c) }
            div("grid") {
                div("first") { c.doer<TxtHtml>().writeText(v?.p_first?.partBody, this, c) }
                div("next") { c.doer<TxtHtml>().writeText(v?.p_next?.partBody, this, c) }
            }
        }
    }

    private fun writeBlocTi(meta: Dba_blocM, o: IDoHtml, c: DoCtx) {
        meta.f_title?.fieldBody?.let { ti ->
            o.div("blockTi") { c.doer<TiHtml>().writePara(ti, this, c) }
        }
    }
}