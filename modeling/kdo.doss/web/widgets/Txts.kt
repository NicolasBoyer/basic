package web.widgets

import eu.scenari.kdo.doer.DoCtx
import eu.scenari.kdo.doer.IDoHtml
import eu.scenari.kdo.doer.binaries.DoerRemoteDescs
import eu.scenari.kdo.doer.binaries.getImgProps
import eu.scenari.kdo.doer.div
import eu.scenari.kdo.doer.tag
import eu.scenari.kdo.doer.web.DoerPara
import eu.scenari.kdo.doer.web.DoerText
import eu.scenari.kdo.material.*
import eu.scenari.kdo.organizers.DoBreadcrumbs
import eu.scenari.kdo.util.cast
import eu.scenari.kdo.util.nullIfEmpty
import model.binary.Bpng_gif_jpg_jpeg
import model.compo.Cba_page
import model.data.Dba_imageM
import model.data.Dba_resM
import model.data.Dba_url
import model.text.LRef_page
import model.text.Tba_title
import web.PagePg

open class TiHtml : DoerPara<Tba_title>() {
	override fun writePara(m: Tba_title?, o: IDoHtml, c: DoCtx) {
		if (m == null) return
		super.writePara(m, o, c)
	}
}

open class TiAsString : DoerPara<Tba_title>() {
	override fun asString(m: Tba_title, c: DoCtx): StringBuilder? {
		return super.asString(m, c)
	}
}

open class TxtHtml : DoerText<TextMultiPara<ITextBlock>>(headRole, headRole) {

	override fun writeText(m: TextMultiPara<ITextBlock>?, o: IDoHtml, c: DoCtx) {
		if (m == null) return
		super.writeText(m, o, c)
	}

	override fun urlToInlineImg(m: TextInlineImg<*, *>, b: IBinary, c: DoCtx): String? {
		return when (b) {
			is Bpng_gif_jpg_jpeg -> c.siteMgr.urlToPage(c.singletonPage(b, c.doer<InlineImg>()))
			else -> TODO()
		}
	}

	override fun writeUlinkRef(m: TextUlinkRef<*, *, *>, o: IDoHtml, c: DoCtx) {
		m.target(c)?.also { target ->
			when (m) {
				is LRef_page -> writeLinkPage(m, target, c, o)
				else -> TODO()
			}
		} ?: m.forEach { writeInline(it, o, c) }
	}

	protected open fun writeLinkPage(m: LRef_page, target: IMaterial, c: DoCtx, o: IDoHtml) {
		target.cast<Cba_page>()?.also { page ->
			o.tag("a", "page") {
				val bcs = c.doer<DoBreadcrumbs>()
				val pgTarget = bcs.findPg(page, c.doer<PagePg>()) { bc -> bc.size == 1 } ?: bcs.getOrAddChildPage(page, c.doer<PagePg>())
				att("href", c.siteMgr.urlToPage(pgTarget))
				m.forEach { writeInline(it, o, c) }
			}
		} ?: m.forEach { writeInline(it, o, c) }
	}

	override fun writePhrase(m: TextPhrase<*, *>, o: IDoHtml, c: DoCtx) {
		m.meta?.also { meta ->
			when (meta) {
				is Dba_url -> {
					o.tag("a", "tph ${classRole(m.role)}") {
						meta.f_url.value.nullIfEmpty()?.let { u ->
							att("href", c.doer<DoerRemoteDescs>().getRemoteUrl(u).url(true))
						}
						att("target", "_blank")
						att("title", meta.f_title?.value)
						m.forEach { writeInline(it, this, c) }
					}
				}

				else -> super.writePhrase(m, o, c)
			}
		} ?: super.writePhrase(m, o, c)
	}


	override fun writeExtBlock(m: TextExtBlock<*, *>, o: IDoHtml, c: DoCtx) {
		m.target(c)?.let { b ->
			when (b) {
				is Bpng_gif_jpg_jpeg -> {
					var title: Tba_title? = null
					val image = b as IBinary
					m.meta?.cast<Dba_resM>()?.let { resM -> title = resM.f_title?.fieldBody }
					if (image.getImgProps().width!! > 600 || image.getImgProps().height!! > 600) {
						o.tag("a") {
							att("mag", m.meta?.cast<Dba_resM>()?.f_isMag?.value ?: "yes")
							att("is", "ba-zoom")
							att("skin", c.siteMgr.urlToStaticRes("styles/gallery.css"))
							att("href", c.siteMgr.urlToPage(c.singletonPage(b, c.doer<LargeImg>())))
							att("aria-label", "Cliquez pour agrandir")
							att("target", "_blank")
							writeImage(m, o, c, image)
						}
					} else writeImage(m, o, c, image)
					b.cast<IBinary>()?.meta(c)?.cast<Dba_imageM>()?.let { imageM ->
						o.div("legend") {
							if (title != null) c.doer<TiHtml>().writePara(title, this, c)
							else txt(imageM.f_title?.value ?: "")
						}
					}
				}

				else -> TODO()
			}
		}
	}

	private fun writeImage(m: TextExtBlock<*, *>, o: IDoHtml, c: DoCtx, image: IBinary) {
		o.tag("img", "teb ${classRole(m.role)}") {
			att("alt", m.meta?.cast<Dba_imageM>()?.f_alt?.value)
			att("data-src", c.siteMgr.urlToPage(c.singletonPage(image, c.doer<MediumBlockImg>())))
		}
	}
}
