package web.widgets

import eu.scenari.kdo.doer.*
import eu.scenari.kdo.material.IBinary
import eu.scenari.kdo.util.cast
import model.compo.*
import model.data.Dba_imageM
import model.data.Dba_sliderM

enum class SliderType {
    FullscreenCarousel, Slider
}

open class Slider : IDoer {
    fun writeSlider(ig: Cba_imagesGallery, o: IDoHtml, c: DoCtx, type: SliderType, options: Dba_sliderM?, url: String = "") {
        o.section ("ba-slider") {
            att("auto-scroll", options?.f_autoScroll?.value ?: "4000")
            att("is", "ba-slider")
            if (url != "") att("link", url)
            when (type) {
                SliderType.FullscreenCarousel -> {
                    att("skin", c.siteMgr.urlToStaticRes("styles/fullscreenCarousel.css"))
                    att("type", "fullscreenCarousel")
                }
                SliderType.Slider -> {
                    att("skin", c.siteMgr.urlToStaticRes("styles/slider.css"))
                    att("type", "slider")
                }
            }
            ig.s_images.forEach { image ->
                o.div {
                    att("hidden", "")
                    elt("img") {
                        val target = image.target(c)
                        val meta = target?.meta(c)?.cast<Dba_imageM>()
                        att("alt", meta?.f_alt?.value ?: meta?.f_title?.value)
                        att("data-src", target?.cast<IBinary>()?.let { pImage -> c.siteMgr.urlToPage(c.singletonPage(pImage, c.doer<LargeImg>())) })
                    }
                }
            }
        }
    }
}