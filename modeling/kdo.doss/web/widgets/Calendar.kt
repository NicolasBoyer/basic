package web.widgets

import eu.scenari.kdo.doer.*
import model.compo.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter

open class Calendar : IDoer {
    fun writeCalendar(cal: Cba_calendar, o: IDoHtml, c: DoCtx) {
        o.elt("table") {
            cal.s_event.forEach { event ->
                writeEvent(event, o, c)
            }
        }
    }

    private fun writeEvent(e: Pevent, o: IDoHtml, c: DoCtx) {
        o.elt("tr") {
            val formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy")
            o.elt("td") { txt(e.meta?.f_date?.value?.let { LocalDate.parse(it) }?.format(formatter) ?: "") }
            o.elt("td") {
                o.div("title") { txt(e.meta?.f_title?.value ?: "") }
                o.div("location") { txt(e.meta?.f_location?.value ?: "") }
            }
            o.elt("td") {
                o.elt("a") {
                    att("target", "_blank")
                    att("href", e.meta?.f_link?.value)
                    o.span { txt("En savoir plus") }
                }
            }
        }
    }
}