package web.widgets

import eu.scenari.kdo.doer.*
import eu.scenari.kdo.doout.DoJsonToAppendable
import eu.scenari.kdo.material.IBinary
import eu.scenari.kdo.util.cast
import model.compo.*
import model.data.Dba_imageM

open class MembersAbout : IDoer {
    fun writeMembers(i: Cba_imagesGallery, o: IDoHtml, c: DoCtx): DoJsonToAppendable<kotlin.text.StringBuilder /* = java.lang.StringBuilder */> {
        return DoJsonToAppendable(StringBuilder()).apply {
            arr {
                i.s_images.forEach { pImage ->
                    obj {
                        keyStr("t", pImage.target(c)?.cast<IBinary>()?.let { image -> image.meta(c)?.cast<Dba_imageM>()?.f_title?.value })
                        keyStr("u", pImage.target(c)?.cast<IBinary>()?.let { image -> c.siteMgr.urlToPage(c.singletonPage(image, c.doer<MembersImg>())) })
                    }
                }
            }
        }
    }
}