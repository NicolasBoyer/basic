package web.widgets

import eu.scenari.kdo.doer.*
import eu.scenari.kdo.doout.DoJsonToAppendable
import eu.scenari.kdo.material.IBinary
import eu.scenari.kdo.util.cast
import model.compo.*
import model.data.Dba_mwUnM

open class VideosGallery : IDoer {
    fun writeVideos(v: Cba_videosGallery, o: IDoHtml, c: DoCtx): DoJsonToAppendable<kotlin.text.StringBuilder /* = java.lang.StringBuilder */> {
        return DoJsonToAppendable(StringBuilder()).apply {
            arr {
                v.s_videos.forEach { pvideo ->
                    obj {
                        keyStr(
                            "u",
                            pvideo.target(c)?.cast<IBinary>()
                                ?.let { video -> c.siteMgr.urlToPage(c.singletonPage(video, c.doer<Video>())) } ?:
                            pvideo.target(c)?.cast<Cba_mediaWeb>()?.p_unknown?.meta?.cast<Dba_mwUnM>()?.f_frag?.value)
                    }
                }
            }
        }
    }
}