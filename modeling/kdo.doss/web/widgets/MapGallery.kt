package web.widgets

import eu.scenari.kdo.doer.*
import eu.scenari.kdo.doer.binaries.getImgProps
import eu.scenari.kdo.doout.DoHtmlToAppendable
import eu.scenari.kdo.doout.DoJsonToAppendable
import eu.scenari.kdo.material.*
import eu.scenari.kdo.util.cast
import model.compo.*
import model.data.Dba_imageM

open class MapGallery : IDoer {
    fun writeJson(g: Cba_mapGallery, o: IDoHtml, c: DoCtx): DoJsonToAppendable<kotlin.text.StringBuilder /* = java.lang.StringBuilder */> {
        return DoJsonToAppendable(StringBuilder()).apply {
            if (g.p_gallery != null) {
                arr {
                    g.p_gallery?.partBody?.s_images?.forEach { part ->
                        writeGalleryJson(part, c, o, this)
                    }
                }
            }
            if (g.p_editoGallery != null) {
                obj {
                    g.p_editoGallery!!.partBody(c).let { cbaEditoGallery ->
                        keyStr("i", cbaEditoGallery?.meta?.f_img?.target(c)?.cast<IBinary>()
                                ?.let { image -> c.siteMgr.urlToPage(c.singletonPage(image, c.doer<LargeImg>())) })
                        val props = cbaEditoGallery?.meta?.f_img?.target(c)?.cast<IBinary>()?.getImgProps()
                        keyStr("is", "${props?.width?.toInt()} ${props?.height?.toInt()}")
                        key("z").arr {
                            cbaEditoGallery?.s_zones?.forEach { pZone ->
                                obj {
                                    keyStr("c", pZone.node?.firstChild?.firstChild?.nextSibling?.firstChild.toString())
                                    pZone.partBody(c)?.let { cbaImagesGallery ->
                                        keyStr("t",
                                            cbaImagesGallery.meta?.f_title?.fieldBody?.let {
                                                c.doer<TiAsString>().asString(it, c)
                                            } ?: ""
                                        )
                                        key("g").arr {
                                            cbaImagesGallery.s_slides.forEach { part ->
                                                writeGalleryJson(part, c, o, this)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun writeGalleryJson (part: IPart, c: DoCtx, o: IDoHtml, json: IDoJson) {
        when (part) {
            is Ptext -> {
                return json.obj {
                    keyStr("text", DoHtmlToAppendable(StringBuilder()).also {
                        c.doer<TxtHtml>().writeText(part.partBody, it, c)
                    }.out)
                }
            }
            is Pimage -> {
                val castBinary = part.target(c)?.cast<IBinary>()
                return json.obj {
                    keyStr("t", castBinary?.let { image -> image.meta(c)?.cast<Dba_imageM>()?.f_title?.value ?: "" })
                    keyStr("a", castBinary?.let { image -> image.meta(c)?.cast<Dba_imageM>()?.f_alt?.value ?: "" })
                    keyStr(
                        "c",
                        castBinary?.let { image -> image.meta(c)?.cast<Dba_imageM>()?.f_coordinates?.value ?: "" })
                    keyStr(
                        "u",
                        castBinary?.let { image ->
                            c.siteMgr.urlToPage(
                                c.singletonPage(
                                    image,
                                    c.doer<GalleryImg>()
                                )
                            )
                        })
                    keyStr(
                        "z",
                        castBinary?.let { image ->
                            c.siteMgr.urlToPage(
                                c.singletonPage(
                                    image,
                                    c.doer<LargeGalleryImg>()
                                )
                            )
                        })
                    keyStr("d", castBinary?.let { image -> image.meta(c)?.cast<Dba_imageM>()?.f_date?.value ?: "" })
                }
            }
        }
    }
}