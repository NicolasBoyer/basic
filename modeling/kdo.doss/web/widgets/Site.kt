package web.widgets

import eu.scenari.kdo.doer.*
import eu.scenari.kdo.dosite.IDoSitePage
import eu.scenari.kdo.material.IBinary
import eu.scenari.kdo.material.IMaterial
import eu.scenari.kdo.organizers.DoBreadcrumbs
import eu.scenari.kdo.util.cast
import model.compo.Cba_imagesGallery
import model.compo.Cba_page
import model.compo.Cba_site
import web.PagePg
import web.SitePg

open class Site : IDoer {

	open fun writeTitle(site: Cba_site?, o: IDoHtml, c: DoCtx) {
		o.elt("title") {
			txt(site?.meta?.f_title?.value ?: "")
		}
	}

	open fun writeHeader(s: Cba_site, o: IDoHtml, c: DoCtx) {
		o.tag("header", null, "header") {
			att("role", "header")
			elt("h1") {
				tag("a") {
					val url = c.siteMgr.urlToPage(getStartPage(s, c))
					att("href", url)
					if (s.meta?.f_logo != null) {
						elt("img") {
							att("alt", s.meta?.f_title?.value)
							att(
								"data-src",
								s.meta?.f_logo?.target(c)?.cast<IBinary>()
									?.let { image -> c.siteMgr.urlToPage(c.singletonPage(image, c.doer<LogoImg>())) })
						}
					} else {
						span { txt(s.meta?.f_title?.value ?: "") }
					}
				}
			}
			writeMenu(s, o, c)
		}
	}

	fun writeBreadcrumb(o: IDoHtml, c: DoCtx) {
		val breadcrumbs = c.doer<DoBreadcrumbs>()
		breadcrumbs.currentBreadcrumb?.let { breadcrumb ->
			if (breadcrumb.size > 1) {
				//On est bien sur une page orpheline avec fil d'ariane parent.
				o.tag("div", "breadcrumb") {
					breadcrumb.forEachIndexed { index, _ ->
						val page = breadcrumbs.getOrAddPageFromBreadcrumbAtDepth(breadcrumb, index + 1)
						val title = page.material.cast<Cba_page>()?.meta?.f_title?.fieldBody
						if (index < breadcrumb.lastIndex) {
							tag("a") {
								att("href", c.siteMgr.urlToPage(page))
								span { title.also { c.doer<TiHtml>().writePara(it, o, c) } }
							}
							span("spacer") {
								span { txt(" > ") }
							}
						} else {
							span("selected") { title.also { c.doer<TiHtml>().writePara(it, o, c) } }
						}
					}
				}
			}
		}
	}

	fun writeMenu(s: Cba_site, o: IDoHtml, c: DoCtx) {
		if (s.s_page.size <= 1) return
		o.div("pageTitle") {
			s.s_page.forEach { page ->
				val target = page.target(c)
				if (target == c.siteMgr.doingPage?.material || target == c.doer<DoBreadcrumbs>().currentBreadcrumb?.get(0)?.m) {
					target?.meta?.f_title?.fieldBody.also { c.doer<TiHtml>().writePara(it, o, c) }
				}
			}
		}
		o.tag("nav", "", "navigation") {
			att("aria-label", "menu principal")
			att("role", "navigation")
			tag("ul") {
				s.s_page.forEach { page ->
					o.elt("li") {
						tag("a") {
							val target = page.target(c)
							if (target == c.siteMgr.doingPage?.material || target == c.doer<DoBreadcrumbs>().currentBreadcrumb?.get(0)?.m) {
								att("class", "btn page selected")
							} else {
								att("class", "btn page")
								if (target != null) att("href", c.siteMgr.urlToPage(c.doer<DoBreadcrumbs>().getOrAddRootPage(target, c.doer<PagePg>())))
							}
							span {
								target?.meta?.f_title?.fieldBody.also { c.doer<TiHtml>().writePara(it, o, c) }
							}
						}
					}
				}
			}
		}
	}

	open fun writeFooter(s: Cba_site?, o: IDoHtml, c: DoCtx) {
		if (s?.meta?.f_socialNetworks != null || s?.meta?.f_copyright != null) {
			o.tag("footer", null, "footer") {
				att("role", "contentinfo")
				div("socialNetwork") {
					s.meta?.f_socialNetworks?.f_socialeNetwork?.forEach { sn ->
						tag("a", "socialNetwork") {
							att("href", sn.f_url.value)
							att("target", "_blank")
							span {
								txt(sn.f_title.value)
							}
						}
					}
				}
				div("copyright") {
					s.meta?.f_copyright?.value?.let { txt(it) }
				}
			}
		}
	}

	fun writePage(s: Cba_site, o: IDoHtml, c: DoCtx) {
		o.html("fr") {
			elt("head") {
				metaCharsetUTF8()
				metaViewport()
				writeTitle(s, this, c)
				c.doer<PagePg>().writeHead(o, c)
			}
			tag("body", if (s.p_home?.target(c)?.tag == "ba:imagesGallery") "home gallery" else "home image") {
				val url = c.siteMgr.urlToPage(getFirstPage(s, c))
				if (s.p_home?.target(c)?.tag == "sfile:png_gif_jpg_jpeg") {
					elt("a") {
						att("href", url)
						elt("img") {
							att("alt", s.meta?.f_title?.value)
							att("data-src", s.p_home?.target(c)?.cast<IBinary>()?.let { image -> c.siteMgr.urlToPage(c.singletonPage(image, c.doer<LargeImg>())) })
						}
					}
				}
				if (s.p_home?.target(c)?.tag == "ba:imagesGallery") {
					s.p_home?.target(c)?.cast<Cba_imagesGallery>().let {
						c.doer<Slider>().writeSlider(it!!, this, c, SliderType.FullscreenCarousel, null, url)
					}
				}
			}
		}
	}

	fun getStartPage(s: Cba_site, c: DoCtx): IDoSitePage<out IMaterial> {
		if (s.p_home != null) {
			return c.singletonPage(s, c.doer<SitePg>())
		}
		return getFirstPage(s, c)
	}

	private fun getFirstPage(s: Cba_site, c: DoCtx): IDoSitePage<Cba_page> {
		val page = s.s_page[0]
		val cbaPage = page.partBody(c) ?: Cba_page(page, null)
		return c.doer<DoBreadcrumbs>().getOrAddRootPage(cbaPage, c.doer<PagePg>())
	}
}
