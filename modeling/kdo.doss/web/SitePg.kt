package web

import eu.scenari.kdo.doer.DoCtx
import eu.scenari.kdo.doer.IDoOut
import eu.scenari.kdo.doer.web.IDoerPageHtml
import eu.scenari.kdo.doout.asHtmlDocWithDataOrigin
import eu.scenari.kdo.dosite.IDoSitePage
import model.compo.Cba_site
import web.widgets.Site

class SitePg : IDoerPageHtml<Cba_site> {
    override fun doPage(sitePage: IDoSitePage<Cba_site>, out: IDoOut, c: DoCtx) {
        c.doer<Site>().writePage(sitePage.material, out.asHtmlDocWithDataOrigin(c), c)
    }
}