package web

import eu.scenari.kdo.doer.*
import eu.scenari.kdo.doer.web.IDoerPageHtml
import eu.scenari.kdo.doout.DoJsToAppendable
import eu.scenari.kdo.doout.asHtmlDocWithDataOrigin
import eu.scenari.kdo.doout.scriptJson
import eu.scenari.kdo.dosite.IDoSitePage
import eu.scenari.kdo.util.DoProps
import eu.scenari.kdo.util.cast
import model.compo.*
import model.data.FOtitle
import web.widgets.*

class PagePg : IDoerPageHtml<Cba_page> {
    override fun doPage(sitePage: IDoSitePage<Cba_page>, out: IDoOut, c: DoCtx) {
        val site = c.start.cast<Cba_site>() //Si la page est publiée dans le contexte d'un site.
        val siteDoer = c.doer<Site>()
        val o = out.asHtmlDocWithDataOrigin(c)
        val p = sitePage.material
        o.html("fr") {
            elt("head") {
                metaCharsetUTF8()
                metaViewport()
                if (site != null) siteDoer.writeTitle(site, this, c)
                writeHead(o, c)
            }
            tag("body", if (p.meta?.f_classes != null) "page ${p.meta?.f_classes?.value}" else "page") {
                if (p.p_slider != null) {
                    writeSlider(p, o, c, site, siteDoer)
                } else if (site != null) {
                    siteDoer.writeHeader(site, this, c)
                }
                writeMain(p, this, c, siteDoer)
                if (site != null) siteDoer.writeFooter(site, this, c)
            }
        }
    }

    fun writeHead(o: IDoHtml, c: DoCtx) {
        o.script(
            DoJsToAppendable(StringBuilder()).also {
                with(it) {
                    line("""window.skinRoot = new URL("${c.siteMgr.urlToStaticRes("/styles")}/", document.baseURI)""")
                    line("""window.libmdRoot = new URL("${c.siteMgr.urlToStaticRes("/")}/", document.baseURI)""")
                    line("""window.siteRoot = new URL("${c.siteMgr.urlToStaticRes("")}/", document.baseURI)""")
                }
            }.out.toString()
        )
        o.linkCss(c.siteMgr.urlToStaticRes("styles/vars.css"))
        o.linkCss(c.siteMgr.urlToStaticRes("styles/core.css"))
        o.elt("script") {
            att("type", "module")
            att("src", c.siteMgr.urlToStaticRes("/lib-md/index.js"))
        }
    }

    private fun writeSlider(p: Cba_page, o: IDoHtml, c: DoCtx, site: Cba_site?, siteDoer: Site) {
        o.tag("header", "slider", "header") {
            att("role", "header")
            p.p_slider?.target(c)?.cast<Cba_imagesGallery>().let {
                c.doer<Slider>().writeSlider(it!!, o, c, SliderType.Slider, p.p_slider?.meta, if (site !== null) c.siteMgr.urlToPage(siteDoer.getStartPage(site, c)) else "")
            }
            if (site != null) siteDoer.writeMenu(site, this, c)
        }
    }

    private fun writeMain(p: Cba_page, o: IDoHtml, c: DoCtx, siteDoer: Site?) {
        o.tag("main", id="main") {
            siteDoer?.writeBreadcrumb(o, c)
            p.s_1.forEach { part ->
                when (part) {
                    is Pmembers -> {
                        o.section("ba-members section") {
                            att("is", "ba-members")
                            att("skin", c.siteMgr.urlToStaticRes("styles/members.css"))
                            part.partBody(c)?.let { writeTitle(it.meta?.f_title, o, c) }
                            val json = part.partBody(c)?.let { c.doer<MembersAbout>().writeMembers(it, this, c) }
                            scriptJson("json") {
                                if (json != null) {
                                    txt(json.out)
                                }
                            }
                        }
                    }

                    is Pcalendar -> {
                        o.section("ba-calendar section") {
                            att("is", "inject-skin")
                            att("skin", c.siteMgr.urlToStaticRes("styles/calendar.css"))
                            part.partBody(c)?.let {
                                writeTitle(it.meta?.f_title, o, c)
                                c.doer<Calendar>().writeCalendar(it, this, c)
                            }
                        }
                    }
                    is Pimages -> {
                        o.section("ba-gallery section") {
                            att("is", "ba-gallery")
                            att("thumbs", part.meta?.f_isThumbs?.value ?: "yes")
                            att("mag", part.meta?.f_isMag?.value ?: "yes")
                            att("skin", c.siteMgr.urlToStaticRes("styles/gallery.css"))
                            part.partBody(c)?.let {
                                writeTitle(it.meta?.f_title, o, c)
                                c.doer<Gallery>().writeGallery(it, this, c)
                            }
                        }
                    }
                    is Pvideos -> {
                        o.section("ba-videos section") {
                            att("is", "ba-videos")
                            att("skin", c.siteMgr.urlToStaticRes("styles/videos.css"))
                            part.partBody(c)?.let { writeTitle(it.meta?.f_title, o, c) }
                            val json = part.partBody(c)?.let { c.doer<VideosGallery>().writeVideos(it, this, c) }
                            scriptJson("json") {
                                if (json != null) {
                                    txt(json.out)
                                }
                            }
                        }
                    }
                    is Ptexts -> {
                        o.section("ba-texts section") {
                            att("is", "inject-skin")
                            att("skin", c.siteMgr.urlToStaticRes("styles/texts.css"))
                            part.partBody(c)?.let {
                                writeTitle(it.meta?.f_title, o, c)
                                c.doer<Texts>().writeTexts(it, this, c)
                            }
                        }
                    }
                    is Pform -> {
                        o.section("ba-form section") {
                            att("is", "ba-form")
                            att("skin", c.siteMgr.urlToStaticRes("styles/form.css"))
                            part.partBody(c)?.let {
                                att("email", it.meta?.f_submitLink?.value)
                                att("subject", it.meta?.f_subject?.value)
                                writeTitle(it.meta?.f_title, o, c)
                                c.doer<Form>().writeForm(part.partBody, this, c)
                            }
                        }
                    }
                    is Ptiles -> {
                        o.section("ba-tiles section") {
                            att("is", "inject-skin")
                            att("skin", c.siteMgr.urlToStaticRes("styles/tiles.css"))
                            part.partBody(c)?.let {
                                writeTitle(it.meta?.f_title, o, c)
                                c.doer<Tiles>().writeTiles(it, this, c)
                            }
                        }
                    }
                    is PmapGallery -> {
                        o.section("ba-map-gallery section") {
                            att("is", part.partBody(c)?.let { if (it.p_gallery != null) "ba-map-gallery" else "ba-custom-map-gallery"})
                            att("thumbs", part.meta?.f_isThumbs?.value ?: "yes")
                            att("mag", part.meta?.f_isMag?.value ?: "yes")
                            att("journey", part.meta?.f_isJourney?.value ?: "yes")
                            att("skin", c.siteMgr.urlToStaticRes("styles/mapGallery.css"))
                            part.partBody(c)?.let { writeTitle(it.meta?.f_title, o, c) }
                            o.div("map") {}
                            val json = part.partBody(c)?.let { c.doer<MapGallery>().writeJson(it, this, c) }
                            scriptJson("json") {
                                if (json != null) {
                                    txt(json.out)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun writeTitle(title: FOtitle?, o: IDoHtml, c: DoCtx) {
        if (title != null) {
            o.tag("h2", "title") {
                span {
                    title.fieldBody.also { c.doer<TiHtml>().writePara(it, o, c) }
                }
            }
        }
    }

    override fun pageFolderHint(m: Cba_page, props: DoProps?, c: DoCtx): String = ""
    override fun pageExtension(m: Cba_page, props: DoProps?, c: DoCtx) = ""
}