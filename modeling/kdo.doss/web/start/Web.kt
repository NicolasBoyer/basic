package web.start

import eu.scenari.kdo.doer.DoCtx
import eu.scenari.kdo.doer.IDoerStart
import eu.scenari.kdo.material.IMaterial
import eu.scenari.kdo.organizers.DoBreadcrumbs
import model.compo.Cba_page
import model.compo.Cba_site
import web.PagePg
import web.widgets.Site

open class Web : IDoerStart {
	override fun start(root: IMaterial, c: DoCtx) {
		when (root) {
			is Cba_site -> {
				c.doer<Site>().getStartPage(root, c)
				//On pré-construit toutes les pages principales du site pour les retrouver
				//en cas de lien dans le texte (et ne pas créer une page orpheline avec fil d'ariane si c'est une page du site)
				val breadcrumbs = c.doer<DoBreadcrumbs>()
				root.s_page.forEach { p ->
					p.partBody(c)?.let { page -> breadcrumbs.getOrAddChildPage(page, c.doer<PagePg>()) }
				}
			}

			is Cba_page -> {
				c.singletonPage(root, c.doer<PagePg>())
			}

			else -> throw Exception("Root not allowed: $root")
		}
	}
}
