package web

import eu.scenari.kdo.doer.DoCtx
import eu.scenari.kdo.doer.IDoOut
import eu.scenari.kdo.doer.binaries.DoerBinary
import eu.scenari.kdo.dosite.IDoSitePage
import eu.scenari.kdo.material.IBinary
import eu.scenari.kdo.util.DoProps

open class BinaryPg<M : IBinary, I : IBinary>(val resDoer: DoerBinary<I>) : DoerBinary<M>() {
    override fun pageFolderHint(m: M, props: DoProps?, c: DoCtx): String = "assets"
    override fun doPage(sitePage: IDoSitePage<M>, out: IDoOut, c: DoCtx) {
        resDoer.doPage(sitePage as IDoSitePage<I>, out, c)
    }
}