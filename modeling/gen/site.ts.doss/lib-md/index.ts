import DynamicImports from './modules/dynamicImports.js'
import { html, nothing, render } from 'lit'
import Utils from './modules/utils.js'

declare global {
    interface Window {skinRoot: URL}
}

class Index {
    constructor() {
        const preloadContent = Array.from(document.querySelectorAll('img'))
        const options = {
            rootMargin: '0px',
            thresold: 0.25,
        }
        const callback = async (entries: IntersectionObserverEntry[], observer: IntersectionObserver) => {
            for (const entry of entries) {
                if (entry.isIntersecting && preloadContent.includes(entry.target as HTMLImageElement)) {
                    const src = entry.target.getAttribute('data-src')
                    if (src) {
                        (entry.target as HTMLImageElement).src = await Utils.urlToBase64(src) as string
                        entry.target.removeAttribute('data-src')
                        observer.unobserve(entry.target)
                    }
                }
            }
            window.dispatchEvent(new CustomEvent('imagesVisible'))
        }
        const observer = new IntersectionObserver(callback, options)
        preloadContent.forEach((pContent) => {
            observer.observe(pContent)
        })
        new DynamicImports()
        const header = document.querySelector('header')
        if (header) {
            render(
                html`
                    <div is="ba-sections"></div>
                    ${document.querySelector('#navigation') ? html`
                        <div is="ba-responsive"></div>` : nothing}
                `,
                header,
            )
        }
    }
}

new Index()
