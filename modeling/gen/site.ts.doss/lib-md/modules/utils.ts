import { html, render } from 'lit'

export default class Utils {
    static async request(pUrl: string, pMethod = 'GET', pOptions: RequestInit = {}, pReturnType = '') {
        const response = await fetch(pUrl, { ...{ method: pMethod }, ...pOptions })
        if (pReturnType === 'status' && pMethod === 'HEAD') return response.status
        if (response.status !== 200 && response.status !== 204) {
            // eslint-disable-next-line no-console
            console.error('Request failed : ' + response.status)
            // eslint-disable-next-line no-console
            console.log(response)
        } else {
            switch (pReturnType) {
            case 'blob':
                return response.blob()
            case 'text':
                return response.text()
            case 'response':
                return response
            default:
                return response.json()
            }
        }
    }

    static urlToBase64(pUrl: string) {
        return new Promise(async (resolve) => {
            const reader = new FileReader()
            reader.onload = () => resolve(reader.result)
            reader.readAsDataURL(await this.request(pUrl, 'GET', null, 'blob'))
        })
    }

    static declareSkin(skin: string) {
        if (!skin || document.querySelector(`link[href*='${skin.substring(skin.lastIndexOf('/')) || skin}']`)) return
        const link = document.createElement('link')
        link.setAttribute('rel', 'stylesheet')
        link.setAttribute('href', skin)
        document.head.appendChild(link)
    }

    static strToDom<T extends HTMLElement>(str: string): T {
        return document.createRange().createContextualFragment(str).firstChild as T
    }

    static idFromString(str: string) {
        let hash = 0
        if (str.length === 0) {
            return hash
        }
        for (let i = 0; i < str.length; i++) {
            const charCode = str.charCodeAt(i)
            hash = ((hash << 5) - hash) + charCode
            hash |= 0
        }
        return hash
    }

    static loader(visible: boolean, element: HTMLElement): void {
        const loader = element.querySelector('.loader')
        if (!visible) {
            loader?.remove()
            return
        }
        if (!loader) {
            render(
                html`
                    <div class="loader">
                        <div class="spinner"></div>
                    </div>
                `,
                element,
            )
        }
    }
}
