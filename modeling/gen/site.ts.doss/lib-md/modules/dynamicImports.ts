export default class DynamicImports {
    constructor() {
        window.addEventListener('ready', () => setTimeout(() => document.body.style.setProperty('opacity', '1'), 50))
        // @ts-ignore
        return new Promise(async () => {
            if (document.querySelector('[is="inject-skin"]'))
                customElements.define('inject-skin', (await import('../components/injectSkin.js')).default, { extends: 'section' })
            if (document.querySelector('[is="ba-form"]'))
                customElements.define('ba-form', (await import('../components/form.js')).default, { extends: 'section' })
            if (document.querySelector('[is="ba-videos"]'))
                customElements.define('ba-videos', (await import('../components/videos.js')).default, { extends: 'section' })
            if (document.querySelector('[is="ba-members"]'))
                customElements.define('ba-members', (await import('../components/members.js')).default, { extends: 'section' })
            if (document.querySelector('[is="ba-slider"]'))
                customElements.define('ba-slider', (await import('../components/slider.js')).default, { extends: 'section' })
            if (document.querySelector('[is="ba-gallery"]'))
                customElements.define('ba-gallery', (await import('../components/gallery.js')).default, { extends: 'section' })
            if (document.querySelector('[is="ba-map-gallery"]'))
                customElements.define('ba-map-gallery', (await import('../components/mapGallery.js')).default, { extends: 'section' })
            if (document.querySelector('[is="ba-custom-map-gallery"]'))
                customElements.define('ba-custom-map-gallery', (await import('../components/customMapGallery.js')).default, { extends: 'section' })
            if (document.querySelector('[is="ba-zoom"]'))
                customElements.define('ba-zoom', (await import('../components/zoom.js')).default, { extends: 'a' })
            if (document.querySelector('[is="ba-responsive"]'))
                customElements.define('ba-responsive', (await import('../components/responsive.js')).default, { extends: 'div' })
            if (document.querySelector('[is="ba-sections"]'))
                customElements.define('ba-sections', (await import('../components/sections.js')).default, { extends: 'div' })
            window.dispatchEvent(new CustomEvent('ready'))
        })
    }
}
