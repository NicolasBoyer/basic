import Gallery from '../components/gallery.js'
import MapGallery from '../components/mapGallery.js'

export default class Images {
    static zoomImages: Record<string, { width: number; height: number; isMag?: boolean }> = {}
    static controller: AbortController
    static isMagOn = false
    static thumbsGallery: Record<string, { touchstartX: number; touchendX: number; imagesTranslate: number[] }> = {}

    static close() {
        this.isMagOn = false
        document.body.style.overflow = 'auto'
        this.controller.abort()
    }

    static setMag(imageSrc: string, isMag: boolean) {
        if (!isMag || !this.zoomImages[imageSrc].isMag) return
        this.isMagOn = !this.isMagOn
    }

    static magMouseMove(event: MouseEvent & TouchEvent) {
        if (!this.isMagOn) return
        const offsetX = event.offsetX || event.touches[0].pageX
        const offsetY = event.offsetY || event.touches[0].pageX
        const parentImage = (event.target as HTMLImageElement).parentElement as HTMLElement
        parentImage.style.backgroundPosition = `${offsetX / parentImage.offsetWidth * 100}% ${offsetY / parentImage.offsetHeight * 100}%`
    }

    static resize(pGalleryWrapper: HTMLDivElement, image: HTMLImageElement) {
        const { width, height } = pGalleryWrapper.querySelector('.container .images').getBoundingClientRect()
        if (width >= height * this.zoomImages[image.src].width / this.zoomImages[image.src].height) {
            const legendHeight = image.parentElement.querySelector('.legend')?.getBoundingClientRect().height || 0
            const imageHeight = Math.min(height - 20 - legendHeight, this.zoomImages[image.src].height)
            image.style.height = `${imageHeight}px`
            image.parentElement.style.height = `${imageHeight + legendHeight}px`
        } else {
            image.style.width = image.parentElement.style.width = `${Math.min(width - 20, this.zoomImages[image.src].width)}px`
            image.style.height = image.parentElement.style.height = ''
        }
        image.style.display = ''
        this.zoomImages[image.src].isMag = this.zoomImages[image.src].width > image.offsetWidth || this.zoomImages[image.src].height > image.offsetWidth * this.zoomImages[image.src].height / this.zoomImages[image.src].width
    }

    static moveThumbsWheelEvent(event: WheelEvent) {
        event.preventDefault()
        if (!event.deltaY) return
        (event.currentTarget as HTMLElement).scrollBy({
            top: 0,
            left: event.deltaY + event.deltaX,
        })
    }

    static moveThumbsTouchStartEvent(event: TouchEvent, parent: HTMLElement, gallery: Gallery | MapGallery) {
        const thumbsGallery = Images.thumbsGallery[String(gallery)]
        thumbsGallery.touchstartX = event.touches[0].clientX
        thumbsGallery.imagesTranslate = []
        parent.querySelectorAll('img').forEach((image) => {
            const translate = new WebKitCSSMatrix(getComputedStyle(image).transform)
            thumbsGallery.imagesTranslate.push(translate.m41)
        })
    }

    static moveThumbsTouchMoveEvent(event: TouchEvent, parent: HTMLElement, gallery: Gallery | MapGallery) {
        const thumbsGallery = Images.thumbsGallery[String(gallery)]
        thumbsGallery.touchendX = event.touches[0].clientX
        const images = parent.querySelectorAll('img')
        let blocked = false
        for (let i = 0; i < images.length; i++) {
            const image = images[i]
            if (i === 0 && image.getBoundingClientRect().x >= parent.getBoundingClientRect().x && thumbsGallery.touchendX > thumbsGallery.touchstartX || i === images.length - 1 && image.getBoundingClientRect().x <= parent.getBoundingClientRect().width - image.getBoundingClientRect().width + parent.getBoundingClientRect().x && thumbsGallery.touchendX < thumbsGallery.touchstartX) {
                blocked = true
            }
        }
        if (blocked) return
        images.forEach((image, index) => {
            image.style.transform = `translate(${(thumbsGallery.imagesTranslate[index] + (event.touches[0].clientX - thumbsGallery.touchstartX))}px, 0)`
        })
    }
}
