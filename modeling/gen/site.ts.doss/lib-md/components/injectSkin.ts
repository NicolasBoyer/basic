import Utils from '../modules/utils.js'

export default class InjectSkin extends HTMLElement {
    constructor() {
        super()
        Utils.declareSkin(this.skin)
    }

    get skin() {
        return this.getAttribute('skin')
    }
}
