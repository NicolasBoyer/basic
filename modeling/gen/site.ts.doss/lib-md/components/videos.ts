import 'vlitejs/vlite.css'
import { html, render } from 'lit'
import InjectSkin from './injectSkin.js'

export default class Videos extends InjectSkin {
    private videos = JSON.parse(this.firstChild.textContent)

    constructor() {
        super()
        this.firstChild.remove()
        if (this.videos.length === 1) this.classList.add('oneVideo')
        render(
            this.videos.map((pVideoPath: { u: string }, pIndex: number) => html`
                ${pVideoPath.u.includes('<iframe') ? html`
                    <div class="iframe">${document.createRange().createContextualFragment(pVideoPath.u)}</div>
                ` : html`
                    <video id="video_${pIndex}" src="${pVideoPath.u}"></video>
                `}
            `),
            this,
        )
        this.initVideos()
    }

    private async initVideos() {
        const Vlitejs = await import('vlitejs')
        for (let i = 0; i < this.videos.length; i++) {
            if (!this.videos[i].u.includes('<iframe')) new Vlitejs.default(`#video_${i}`)
        }
    }
}
