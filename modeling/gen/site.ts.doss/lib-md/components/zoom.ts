import { html, nothing, render } from 'lit'
import Images from '../modules/images.js'
import Utils from '../modules/utils.js'

export default class Zoom extends HTMLAnchorElement {
    private isHidden = true
    private isMag: boolean
    private wrapper: HTMLDivElement

    constructor() {
        super()
        Utils.declareSkin(this.skin)
        this.isMag = this.mag === 'yes'
        const zoomImage = new Image()
        zoomImage.onload = () => {
            Images.zoomImages[this.href] = { height: zoomImage.naturalHeight, width: zoomImage.naturalWidth }
        }
        zoomImage.src = this.href
        this.addEventListener('click', (event) => {
            event.preventDefault()
            this.isHidden = false
            document.body.style.overflow = 'hidden'
            this.render()
            this.wrapper.style.display = ''
            Images.controller = new AbortController()
            document.body.addEventListener('keyup', (event) => this.setKeyboardEvent(event))
            setTimeout(() => this.resizeZoomedImage(), 50)
        })
        this.wrapper = document.createElement('div')
        this.wrapper.style.display = 'none'
        document.body.appendChild(this.wrapper)
        this.render()
        window.addEventListener('resize', () => this.resizeZoomedImage())
    }

    get skin() {
        return this.getAttribute('skin')
    }

    get mag() {
        return this.getAttribute('mag')
    }

    private setKeyboardEvent(event: KeyboardEvent) {
        if (event.key === 'Escape') {
            this.isHidden = true
            Images.close()
            this.render()
        }
    }

    private resizeZoomedImage() {
        Images.resize(this.wrapper, this.wrapper.querySelector('.images img'))
        this.render()
    }

    private render() {
        render(
            html`
                <div class="container" ?hidden="${this.isHidden}" @click="${() => {
                    this.isHidden = true
                    Images.close()
                    this.render()
                }}">
                    <div class="hover"></div>
                    <div class="tools">
                        <button class="close" role="button" title="￼Fermer la fenêtr (Echap)￼" @click="${(event: PointerEvent) => {
                            event.stopPropagation()
                            this.isHidden = true
                            Images.close()
                            this.render()
                        }}" aria-label="￼Fermer la fenêtre￼">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"
                            ">
                            <path
                                d="M31.708 25.708c-0-0-0-0-0-0l-9.708-9.708 9.708-9.708c0-0 0-0 0-0 0.105-0.105 0.18-0.227 0.229-0.357 0.133-0.356 0.057-0.771-0.229-1.057l-4.586-4.586c-0.286-0.286-0.702-0.361-1.057-0.229-0.13 0.048-0.252 0.124-0.357 0.228 0 0-0 0-0 0l-9.708 9.708-9.708-9.708c-0-0-0-0-0-0-0.105-0.104-0.227-0.18-0.357-0.228-0.356-0.133-0.771-0.057-1.057 0.229l-4.586 4.586c-0.286 0.286-0.361 0.702-0.229 1.057 0.049 0.13 0.124 0.252 0.229 0.357 0 0 0 0 0 0l9.708 9.708-9.708 9.708c-0 0-0 0-0 0-0.104 0.105-0.18 0.227-0.229 0.357-0.133 0.355-0.057 0.771 0.229 1.057l4.586 4.586c0.286 0.286 0.702 0.361 1.057 0.229 0.13-0.049 0.252-0.124 0.357-0.229 0-0 0-0 0-0l9.708-9.708 9.708 9.708c0 0 0 0 0 0 0.105 0.105 0.227 0.18 0.357 0.229 0.356 0.133 0.771 0.057 1.057-0.229l4.586-4.586c0.286-0.286 0.362-0.702 0.229-1.057-0.049-0.13-0.124-0.252-0.229-0.357z"></path>
                            </svg>
                        </button>
                    </div>
                    <div class="images">
                        <div
                            @mousemove="${Images.isMagOn ? (event: MouseEvent & TouchEvent) => Images.magMouseMove(event) : nothing}"
                            class="${this.isMag && Images.zoomImages[this.href]?.isMag ? 'isMag' : ''} ${Images.isMagOn ? 'isMagOn' : ''}"
                            style="${Images.isMagOn ? `background-image:url(${this.href})` : ''}"
                            @click="${(event: PointerEvent) => {
                                event.stopPropagation()
                                Images.setMag(this.href, this.isMag)
                                this.render()
                            }}"
                        >
                            <img style="display: none" src="${this.href}" alt="" />
                        </div>
                    </div>
                </div>
            `,
            this.wrapper,
        )
    }
}
