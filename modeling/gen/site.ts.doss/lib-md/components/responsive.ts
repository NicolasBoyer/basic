import { html, nothing, render } from 'lit'

export default class Responsive extends HTMLDivElement {
    private mnuOpen = false
    private menu: HTMLElement
    private isResponsive = false
    private controller: AbortController

    connectedCallback() {
        this.menu = document.querySelector('#navigation')
        window.addEventListener('resize', () => this.render())
        this.render()
    }

    private toggleMenu() {
        this.mnuOpen = !this.mnuOpen
        const navigation = document.querySelector('#navigation')
        this.render()
        if (this.mnuOpen) {
            navigation.classList.add('open')
            this.controller = new AbortController()
            const { signal } = this.controller
            document.body.addEventListener('keyup', (event) => {
                if (event.key === 'Escape' && this.mnuOpen) this.toggleMenu()
            }, { signal })
            return
        }
        navigation.classList.remove('open')
        this.controller.abort()
    }

    private render() {
        if (!this.menu) return
        this.isResponsive = window.matchMedia('(max-width: 900px)').matches
        render(
            this.isResponsive ? html`
                <button role="button" class="responsiveButton" title="￼Ouvrir le menu￼" aria-label="￼Ouvrir le menu￼" @click="${() => this.toggleMenu()}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="28" viewBox="0 0 24 28">
                        <path
                            d="M24 21v2c0 0.547-0.453 1-1 1h-22c-0.547 0-1-0.453-1-1v-2c0-0.547 0.453-1 1-1h22c0.547 0 1 0.453 1 1zM24 13v2c0 0.547-0.453 1-1 1h-22c-0.547 0-1-0.453-1-1v-2c0-0.547 0.453-1 1-1h22c0.547 0 1 0.453 1 1zM24 5v2c0 0.547-0.453 1-1 1h-22c-0.547 0-1-0.453-1-1v-2c0-0.547 0.453-1 1-1h22c0.547 0 1 0.453 1 1z"></path>
                    </svg>
                </button>
                <div class="${this.mnuOpen ? 'open' : ''} responsiveHover" @click="${() => this.toggleMenu()}"></div>
            ` : nothing,
            this,
        )
        render(
            this.isResponsive ? html`
                <button role="button" class="closeButton" title="￼Fermer le menu￼" aria-label="￼Fermer le menu￼" @click="${() => this.toggleMenu()}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                        <path
                            d="M31.708 25.708c-0-0-0-0-0-0l-9.708-9.708 9.708-9.708c0-0 0-0 0-0 0.105-0.105 0.18-0.227 0.229-0.357 0.133-0.356 0.057-0.771-0.229-1.057l-4.586-4.586c-0.286-0.286-0.702-0.361-1.057-0.229-0.13 0.048-0.252 0.124-0.357 0.228 0 0-0 0-0 0l-9.708 9.708-9.708-9.708c-0-0-0-0-0-0-0.105-0.104-0.227-0.18-0.357-0.228-0.356-0.133-0.771-0.057-1.057 0.229l-4.586 4.586c-0.286 0.286-0.361 0.702-0.229 1.057 0.049 0.13 0.124 0.252 0.229 0.357 0 0 0 0 0 0l9.708 9.708-9.708 9.708c-0 0-0 0-0 0-0.104 0.105-0.18 0.227-0.229 0.357-0.133 0.355-0.057 0.771 0.229 1.057l4.586 4.586c0.286 0.286 0.702 0.361 1.057 0.229 0.13-0.049 0.252-0.124 0.357-0.229 0-0 0-0 0-0l9.708-9.708 9.708 9.708c0 0 0 0 0 0 0.105 0.105 0.227 0.18 0.357 0.229 0.356 0.133 0.771 0.057 1.057-0.229l4.586-4.586c0.286-0.286 0.362-0.702 0.229-1.057-0.049-0.13-0.124-0.252-0.229-0.357z"></path>
                    </svg>
                </button>
            ` : nothing,
            this.menu,
        )
    }
}
