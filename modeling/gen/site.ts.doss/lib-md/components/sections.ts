import { html, render } from 'lit'

export default class Sections extends HTMLDivElement {
    private titles: HTMLElement[] = []

    connectedCallback() {
        const sections = document.querySelectorAll('.section')
        sections.forEach((section) => {
            const title = section.querySelector('h2')
            if (title) this.titles.push(title)
        })
        if (this.titles.length <= 1) return
        this.buildMenu()
    }

    buildMenu() {
        this.titles.forEach((title, index) => title.setAttribute('id', `section_${index}`))
        render(
            html`
                <nav id="sectionMenu" aria-label="menu interne de la page">
                    <ul>
                        ${this.titles.map((title, index) => {
                            const id = `#section_${index}`
                            return html`
                                <li>
                                    <a href="${id}" @click="${(event: MouseEvent) => this.gotoSection(id, event)}">
                                        <span>${title.innerText}</span>
                                    </a>
                                </li>
                            `
                        })}
                    </ul>
                </nav>
            `, this,
        )
    }

    gotoSection(id: string, event: MouseEvent) {
        event.preventDefault()
        const rootElement = document.querySelector('#header') as HTMLElement
        const target = document.querySelector(id) as HTMLElement
        const targetTop = target.style.top
        target.style.position = 'relative'
        target.style.top = getComputedStyle(rootElement).position === 'static' ? '0' : `-${rootElement.offsetHeight}px`
        target.scrollIntoView({ behavior: 'smooth' })
        target.style.top = targetTop
    }
}
