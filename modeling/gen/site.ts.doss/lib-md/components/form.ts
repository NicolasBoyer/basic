import { html, render } from 'lit'
import InjectSkin from './injectSkin.js'

export default class Form extends InjectSkin {
    private form: HTMLFormElement

    connectedCallback() {
        const mail = this.getAttribute('email')
        const subject = this.getAttribute('subject')
        this.removeAttribute('email')
        this.removeAttribute('subject')
        this.form = this.querySelector('form')
        this.render()
        const inputs = this.form.querySelectorAll('input,textarea')
        this.form.addEventListener('submit', async (pEvent) => {
            pEvent.preventDefault()
            let body = ''
            inputs.forEach(pInput => {
                let title = pInput.parentElement.querySelector('label').innerHTML
                title = title.includes(' *') ? title.slice(0, -2) : title
                body += `${title}: ${(pInput as HTMLInputElement).value}\n`
            })
            location.href = `mailto:${mail}?subject=${subject}&body=${encodeURIComponent(body)}`
        })
    }

    private render() {
        render(
            html`
                <div class="submitButton">
                    <button type="submit">￼Envoyer￼</button>
                </div>
            `, this.form,
        )
    }
}
