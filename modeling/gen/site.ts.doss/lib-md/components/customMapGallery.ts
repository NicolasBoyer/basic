import 'leaflet/dist/leaflet.css'
import InjectSkin from './injectSkin.js'
import L from 'leaflet'
import Utils from '../modules/utils.js'
import Gallery from './gallery.js'
import { toJpeg } from 'html-to-image'

export default class CustomMapGallery extends InjectSkin {
    private locations: { coords: string, title: string, id: number }[] = []
    private image: string
    private originalImageSize: { width: number, height: number }
    private subGallery: Record<string, Record<string, string>[]> = {}
    private isJourney: boolean

    get thumbs() {
        return this.getAttribute('thumbs')
    }

    get mag() {
        return this.getAttribute('mag')
    }

    get journey() {
        return this.getAttribute('journey')
    }

    async connectedCallback() {
        this.isJourney = this.journey === 'yes'
        const jsonDiv = this.querySelector('script')
        const json = JSON.parse(jsonDiv.textContent)
        this.image = json.i
        this.originalImageSize = {
            width: json.is.split(' ')[0],
            height: json.is.split(' ')[1],
        }
        for (const zone of json.z) {
            const id = Utils.idFromString(zone.c)
            this.subGallery[id] = zone.g
            for (const subGalleryElement of this.subGallery[id]) {
                if (subGalleryElement.text) {
                    subGalleryElement.text = subGalleryElement.text.replaceAll('data-src', 'src')
                    Utils.loader(true, this)
                    const tempDiv = document.createElement('div')
                    tempDiv.className = 'temp'
                    tempDiv.innerHTML = subGalleryElement.text
                    document.body.appendChild(tempDiv)
                    const element = document.querySelector('.temp') as HTMLElement
                    let dataUrl = await toJpeg(element)
                    element.style.position = 'absolute'
                    element.style.maxWidth = '900px'
                    element.style.width = `${element.getBoundingClientRect().width}px`
                    element.style.height = `${element.getBoundingClientRect().height}px`
                    element.style.position = ''
                    element.style.padding = '1em'
                    const options = {
                        backgroundColor: '#fff',
                    }
                    dataUrl = await toJpeg(element, options)
                    subGalleryElement.z = dataUrl
                    subGalleryElement.u = dataUrl
                    element.remove()
                }
            }
            this.locations.push({ coords: zone.c, title: zone.t, id })
        }
        jsonDiv.remove()
        this.initMap()
    }

    private initMap() {
        const image = new Image()
        image.onload = async () => {
            const map = L.map(this.querySelector('.map') as HTMLElement, {
                minZoom: -2,
                maxZoom: 1,
                center: [0, -image.naturalWidth],
                zoom: 0,
                crs: L.CRS.Simple,
            })
            const southWest = map.unproject([0, image.naturalHeight], map.getMaxZoom() - 1)
            const northEast = map.unproject([image.naturalWidth, 0], map.getMaxZoom() - 1)
            const bounds = new L.LatLngBounds(southWest, northEast)
            L.imageOverlay(this.image, bounds).addTo(map)
            map.setMaxBounds(bounds)
            const markers: L.Layer[] = []
            const latlngs: L.LatLng[] = []
            for (let index = 0; index < this.locations.length; index++) {
                const location = this.locations[index]
                const splitLocation = location.coords.split(',')
                const icon = L.icon({
                    iconUrl: `${window.skinRoot.href}images/marker-icon.png`,
                    iconSize: [25, 41],
                    iconAnchor: [12, 40],
                    popupAnchor: [1, -43],
                    shadowUrl: `${window.skinRoot.href}images/marker-shadow.png`,
                    shadowSize: [41, 41],
                    shadowAnchor: [12, 40],
                })
                let x = parseFloat(splitLocation[0]) + (parseFloat(splitLocation[2]) - parseFloat(splitLocation[0])) / 2
                x = x * image.naturalWidth / this.originalImageSize.width
                let y = parseFloat(splitLocation[1]) + (parseFloat(splitLocation[3]) - parseFloat(splitLocation[1])) / 2
                y = y * image.naturalHeight / this.originalImageSize.height
                const marker = L.marker(map.containerPointToLatLng([x, y]), { icon })
                const refGallery = `_${Math.random().toString().substring(2)}`
                Gallery.customGallery[refGallery] = []
                for (const i of this.subGallery[location.id]) {
                    const url = await Utils.urlToBase64(i.u)
                    Gallery.customGallery[refGallery].push(Utils.strToDom(`<a aria-label="Cliquez pour agrandir" target="_blank" href="${i.z}" title="${i.t || location.title}">
						<img class="map-gallery-popup" alt="${i.a}" src="${url}"/>
					</a>`))
                }
                marker
                    .bindPopup(L.popup({
                        maxWidth: 200,
                        maxHeight: 200,
                    }).setContent(() => {
                        const content = Utils.strToDom(`<div>
						<div class="title">${location.title}</div>
						<section id="_${index}" refGallery="${refGallery}" is="ba-gallery" skin="${window.skinRoot.href}gallery.css" thumbs="${this.thumbs}" mag="${this.mag}" journey="${this.journey}"/>
					</div>`)
                        content.querySelector('section').appendChild(Gallery.customGallery[refGallery][0])
                        return content
                    }))
                    .on('click', async () => this.initBaGallery())
                latlngs.push(marker.getLatLng())
                markers.push(marker)
            }
            if (this.isJourney) L.polyline(latlngs, { color: '#2b82cb' }).addTo(map)
            const group = L.featureGroup(markers).addTo(map)
            map.fitBounds(group.getBounds())
        }
        image.src = this.image
        Utils.loader(false, this)
    }

    private async initBaGallery() {
        if (!customElements.get('ba-gallery')) customElements.define('ba-gallery', (await import('../components/gallery.js')).default, { extends: 'section' })
    }
}
