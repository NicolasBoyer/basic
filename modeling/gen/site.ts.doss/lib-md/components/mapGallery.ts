import 'leaflet/dist/leaflet.css'
import InjectSkin from './injectSkin.js'
import L from 'leaflet'
import Utils from '../modules/utils.js'
import ExifReader from 'exifreader'
import Gallery from './gallery.js'
import { html, render } from 'lit'
import Images from '../modules/images.js'

export default class MapGallery extends InjectSkin {
    private locations: { coords: string, url: string, zoom: string, title: string, alt: string, sortDate: string, date: string }[] = []
    private isJourney: boolean
    private refGallery: string

    get thumbs() {
        return this.getAttribute('thumbs')
    }

    get mag() {
        return this.getAttribute('mag')
    }

    get journey() {
        return this.getAttribute('journey')
    }

    async connectedCallback() {
        Images.thumbsGallery[String(this)] = {
            touchstartX: 0,
            touchendX: 0,
            imagesTranslate: [],
        }
        this.isJourney = this.journey === 'yes'
        const jsonDiv = this.querySelector('script')
        for (const image of JSON.parse(jsonDiv.textContent)) {
            if (image.c) this.locations.push({
                coords: image.c,
                url: image.u,
                zoom: image.z,
                title: image.t,
                alt: image.a,
                sortDate: `${image.d?.replaceAll('-', '') || new Date().toLocaleDateString().split('/').reverse().join('')}000000`,
                date: (image.d ? new Date(image.d) : new Date()).toLocaleDateString(),
            })
            else {
                const req = await Utils.urlToBase64(image.u)
                const infos = await ExifReader.load(req, { expanded: true })
                const exifDate = infos.exif?.DateTimeOriginal?.description
                const arrayDate = exifDate?.split(' ')[0].split(':')
                const sortDate = exifDate?.replaceAll(':', '').replaceAll(' ', '')
                const gps = infos.gps
                if (gps) this.locations.push({ coords: `${gps.Latitude},${gps.Longitude}`, url: image.u, zoom: image.z, title: image.t, alt: image.a, sortDate, date: arrayDate.reverse().join('/') })
                else console.error(`Attention: ${image.u} - ${image.t} n'a pas de coordonnées GPS`)
            }
        }
        this.locations.sort((a, b) => parseInt(a.sortDate) - parseInt(b.sortDate))
        jsonDiv.remove()
        this.initMap()
    }

    private async initMap() {
        this.refGallery = `_${Math.random().toString().substring(2)}`
        Gallery.customGallery[this.refGallery] = []
        const map = L.map(this.querySelector('.map') as HTMLElement)
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        }).addTo(map)
        const markers: L.Layer[] = []
        const latlngs: L.LatLng[] = []
        for (let index = 0; index < this.locations.length; index++) {
            const location = this.locations[index]
            const splitLocation = location.coords.split(',')
            const icon = L.icon({
                iconUrl: `${window.skinRoot.href}images/marker-icon.png`,
                iconSize: [25, 41],
                iconAnchor: [12, 40],
                popupAnchor: [1, -43],
                shadowUrl: `${window.skinRoot.href}images/marker-shadow.png`,
                shadowSize: [41, 41],
                shadowAnchor: [12, 40],
            })
            const marker = L.marker([parseFloat(splitLocation[0]), parseFloat(splitLocation[1])], { icon })
            const url = await Utils.urlToBase64(location.url)
            Gallery.customGallery[this.refGallery].push(Utils.strToDom(`<a aria-label="Cliquez pour agrandir" target="_blank" href="${location.zoom}" title="${location.title ? `${location.title} - ${location.date}` : location.date}">
				<img class="map-gallery-popup" alt="${location.alt || location.title}" src="${url}"/>
			</a>`))
            marker
                .bindPopup(L.popup({
                    maxWidth: 200,
                    maxHeight: 200,
                }).setContent(() => {
                    const content = Utils.strToDom(`<div>
						<div class="title">${location.title}</div>
						<div class="date">${location.date}</div>
						<section id="_${index}" refGallery="${this.refGallery}" is="ba-gallery" skin="${window.skinRoot.href}gallery.css" thumbs="${this.thumbs}" mag="${this.mag}" journey="${this.journey}"/>
					</div>`)
                    content.querySelector('section').appendChild(Gallery.customGallery[this.refGallery][index])
                    return content
                }))
                .on('click', async () => this.initBaGallery())
            latlngs.push(marker.getLatLng())
            markers.push(marker)
        }
        if (this.isJourney) L.polyline(latlngs, { color: '#2b82cb' }).addTo(map)
        const group = L.featureGroup(markers).addTo(map)
        map.fitBounds(group.getBounds())
        this.renderThumbs()
        this.initBaGallery()
    }

    private renderThumbs() {
        render(
            html`
                <section class="map-gallery-thumbs" style="margin-top: 5px;" refGallery="${this.refGallery}" is="ba-gallery" skin="${window.skinRoot.href}gallery.css" thumbs="${this.thumbs}" mag="${this.mag}"
                         @wheel="${(event: WheelEvent) => Images.moveThumbsWheelEvent(event)}"
                         @touchstart="${(event: TouchEvent) => Images.moveThumbsTouchStartEvent(event, (event.target as HTMLElement).parentElement.parentElement, this)}"
                         @touchmove="${(event: TouchEvent) => Images.moveThumbsTouchMoveEvent(event, (event.target as HTMLElement).parentElement.parentElement, this)}"
                >
                    ${Gallery.customGallery[this.refGallery].map((link) => {
                        const newLink = link.cloneNode(true)
                        newLink.addEventListener('click', (event) => {
                            event.preventDefault()
                            link.click()
                        })
                        return newLink
                    })}
                </section>
            `,
            this,
        )
    }

    private async initBaGallery() {
        if (!customElements.get('ba-gallery')) customElements.define('ba-gallery', (await import('../components/gallery.js')).default, { extends: 'section' })
    }
}
