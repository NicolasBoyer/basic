import InjectSkin from './injectSkin.js'
import { html, nothing, render } from 'lit'
import Images from '../modules/images.js'

export default class Gallery extends InjectSkin {
    static isAdaptedImage = true
    static customGallery: Record<string, HTMLAnchorElement[]> = {}
    private isHidden = true
    private links: HTMLAnchorElement[] = []
    private galleryImages: HTMLImageElement[] = []
    private currentImage: number
    private galleryImageHeight: Record<string, number> = {}
    private imageGalleryPadding = 6
    private touchstartX = 0
    private touchendX = 0
    private wrapper: HTMLDivElement
    private isThumbs: boolean
    private isMag: boolean

    get thumbs() {
        return this.getAttribute('thumbs')
    }

    get mag() {
        return this.getAttribute('mag')
    }

    get refGallery() {
        return this.getAttribute('refGallery')
    }

    connectedCallback() {
        Images.thumbsGallery[String(this)] = {
            touchstartX: 0,
            touchendX: 0,
            imagesTranslate: [],
        }
        this.isThumbs = this.thumbs === 'yes'
        this.isMag = this.mag === 'yes'
        ;(this.refGallery && Gallery.customGallery[this.refGallery] || this.querySelectorAll('a')).forEach((link, index) => {
            this.galleryImages.push(link.querySelector('img') as HTMLImageElement)
            this.links.push(link)
            const zoomImage = new Image()
            zoomImage.onload = () => {
                Images.zoomImages[link.href] = { height: zoomImage.naturalHeight, width: zoomImage.naturalWidth }
            }
            zoomImage.src = link.href
            link.addEventListener('click', (event) => {
                event.preventDefault()
                this.currentImage = index
                this.isHidden = false
                document.body.style.overflow = 'hidden'
                this.render()
                this.wrapper.style.display = ''
                Images.controller = new AbortController()
                document.body.addEventListener('keyup', (event) => this.setKeyboardEvent(event))
                setTimeout(() => {
                    this.resizeZoomedImages()
                    this.selectThumbInView()
                }, 50)
            })
        })
        window.addEventListener('imagesVisible', () => this.initGallerySize())
        this.wrapper = document.querySelector(`#wrapper${this.refGallery}`)
        if (!this.wrapper) {
            this.wrapper = document.createElement('div')
            if (this.refGallery) this.wrapper.id = `wrapper${this.refGallery}`
            this.wrapper.style.display = 'none'
            document.body.appendChild(this.wrapper)
        }
        this.render()
        this.style.setProperty('--imageGalleryPadding', `${this.imageGalleryPadding / 2 - 1}px`)
        window.addEventListener('resize', () => {
            this.initGallerySize()
            this.resizeZoomedImages()
        })
    }

    private initGallerySize() {
        const galleryBlock = document.querySelector('.gallery')
        if (!Gallery.isAdaptedImage || !galleryBlock) return
        const availableWidth = galleryBlock.getBoundingClientRect().width
        let ratio = 0
        let maxImagesPerRow = 0
        let rowWidth = 0
        this.galleryImages.forEach((image, index) => {
            ratio += (image.naturalWidth - this.imageGalleryPadding) / (image.naturalHeight - this.imageGalleryPadding)
            rowWidth += image.naturalWidth - this.imageGalleryPadding
            maxImagesPerRow++
            if (rowWidth >= availableWidth) {
                for (let l = index - (maxImagesPerRow - 1); l <= index; l++) {
                    this.galleryImageHeight[this.galleryImages[l].src] = (availableWidth - maxImagesPerRow - this.imageGalleryPadding) / ratio
                }
                ratio = 0
                rowWidth = 0
                maxImagesPerRow = 0
            }
        })
        this.galleryImages.forEach((image) => {
            if (this.galleryImageHeight[image.src]) {
                image.style.height = this.galleryImageHeight[image.src] + 'px'
                image.style.width = 'auto'
            } else image.style.height = 'auto'
        })
    }

    private resizeZoomedImages(onlyCurrentImage = false) {
        const images = this.wrapper.querySelectorAll('.images img')
        this.render()
        if (onlyCurrentImage) Images.resize(this.wrapper, images[this.currentImage] as HTMLImageElement)
        else images.forEach(async (image: HTMLImageElement) => Images.resize(this.wrapper, image))
    }

    private setKeyboardEvent(event: KeyboardEvent) {
        if (event.key === 'Escape') {
            this.isHidden = true
            Images.close()
            this.render()
        }
        if (event.key === 'ArrowLeft') this.left()
        if (event.key === 'ArrowRight') this.right()
    }

    private selectThumbInView() {
        document.querySelector('.thumbs > img.selected')?.scrollIntoView()
    }

    private left() {
        if (this.currentImage > 0) this.currentImage = this.currentImage - 1
        Images.isMagOn = false
        this.resizeZoomedImages(true)
        this.selectThumbInView()
    }

    private right() {
        if (this.currentImage < this.galleryImages.length - 1) this.currentImage = this.currentImage + 1
        Images.isMagOn = false
        this.resizeZoomedImages(true)
        this.selectThumbInView()
    }

    private render() {
        render(
            html`
                <div class="container" ?hidden="${this.isHidden}" @click="${() => {
                    this.isHidden = true
                    Images.close()
                    this.render()
                }}">
                    <div class="hover"></div>
                    <div class="tools">
                        <button class="close" role="button" title="￼Fermer la fenêtre (Echap)￼" @click="${(event: PointerEvent) => {
                            event.stopPropagation()
                            this.isHidden = true
                            Images.close()
                            this.render()
                        }}" aria-label="￼Fermer la fenêtre￼">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"
                            ">
                            <path
                                d="M31.708 25.708c-0-0-0-0-0-0l-9.708-9.708 9.708-9.708c0-0 0-0 0-0 0.105-0.105 0.18-0.227 0.229-0.357 0.133-0.356 0.057-0.771-0.229-1.057l-4.586-4.586c-0.286-0.286-0.702-0.361-1.057-0.229-0.13 0.048-0.252 0.124-0.357 0.228 0 0-0 0-0 0l-9.708 9.708-9.708-9.708c-0-0-0-0-0-0-0.105-0.104-0.227-0.18-0.357-0.228-0.356-0.133-0.771-0.057-1.057 0.229l-4.586 4.586c-0.286 0.286-0.361 0.702-0.229 1.057 0.049 0.13 0.124 0.252 0.229 0.357 0 0 0 0 0 0l9.708 9.708-9.708 9.708c-0 0-0 0-0 0-0.104 0.105-0.18 0.227-0.229 0.357-0.133 0.355-0.057 0.771 0.229 1.057l4.586 4.586c0.286 0.286 0.702 0.361 1.057 0.229 0.13-0.049 0.252-0.124 0.357-0.229 0-0 0-0 0-0l9.708-9.708 9.708 9.708c0 0 0 0 0 0 0.105 0.105 0.227 0.18 0.357 0.229 0.356 0.133 0.771 0.057 1.057-0.229l4.586-4.586c0.286-0.286 0.362-0.702 0.229-1.057-0.049-0.13-0.124-0.252-0.229-0.357z"></path>
                            </svg>
                        </button>
                        ${this.currentImage > 0 ? html`
                            <button class="left" role="button" title="￼Image précéndente (Flêche gauche)￼" @click="${(event: PointerEvent) => {
                                event.stopPropagation()
                                this.left()
                            }}" aria-label="￼Image précéndente￼">
                                <svg xmlns="http://www.w3.org/2000/svg" width="21" height="28" viewBox="0 0 21 28">
                                    <path
                                        d="M18.297 4.703l-8.297 8.297 8.297 8.297c0.391 0.391 0.391 1.016 0 1.406l-2.594 2.594c-0.391 0.391-1.016 0.391-1.406 0l-11.594-11.594c-0.391-0.391-0.391-1.016 0-1.406l11.594-11.594c0.391-0.391 1.016-0.391 1.406 0l2.594 2.594c0.391 0.391 0.391 1.016 0 1.406z"></path>
                                </svg>
                            </button>
                        ` : nothing}
                        ${this.currentImage < this.galleryImages.length - 1 ? html`
                            <button class="right" role="button" title="￼Image suivante (Flêche droite)￼" @click="${(event: PointerEvent) => {
                                event.stopPropagation()
                                this.right()
                            }}" aria-label="￼Image suivante￼">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="28" viewBox="0 0 19 28">
                                    <path
                                        d="M17.297 13.703l-11.594 11.594c-0.391 0.391-1.016 0.391-1.406 0l-2.594-2.594c-0.391-0.391-0.391-1.016 0-1.406l8.297-8.297-8.297-8.297c-0.391-0.391-0.391-1.016 0-1.406l2.594-2.594c0.391-0.391 1.016-0.391 1.406 0l11.594 11.594c0.391 0.391 0.391 1.016 0 1.406z"></path>
                                </svg>
                            </button>
                        ` : nothing}
                    </div>
                    <div class="images">
                        ${this.links.map((link, index) => html`
                            <div
                                @mousemove="${Images.isMagOn ? (event: MouseEvent & TouchEvent) => Images.magMouseMove(event) : nothing}"
                                class="${this.isMag && Images.zoomImages[link.href]?.isMag ? 'isMag' : ''} ${Images.isMagOn ? 'isMagOn' : ''}"
                                style="${Images.isMagOn ? `background-image:url(${link.href})` : ''}"
                                @touchstart="${(event: TouchEvent) => {
                                    this.touchstartX = event.changedTouches[0].screenX
                                }}"
                                @touchend="${(event: TouchEvent) => {
                                    this.touchendX = event.changedTouches[0].screenX
                                    if (this.touchendX < this.touchstartX) this.right()
                                    if (this.touchendX > this.touchstartX) this.left()
                                }}"
                                @click="${(event: PointerEvent) => {
                                    event.stopPropagation()
                                    Images.setMag(link.href, this.isMag)
                                    this.render()
                                }}"
                            >
                                <img ?hidden="${this.currentImage !== index}" style="display: none" src="${link.href}" alt="${link.querySelector('img').alt}" />
                                <div ?hidden="${this.currentImage !== index}" class="legend">${link.title}</div>
                            </div>
                        `)}
                    </div>
                    ${this.isThumbs ? html`
                        <div class="thumbs"
                             @wheel="${(event: WheelEvent) => Images.moveThumbsWheelEvent(event)}"
                             @touchstart="${(event: TouchEvent) => Images.moveThumbsTouchStartEvent(event, (event.target as HTMLElement).parentElement, this)}"
                             @touchmove="${(event: TouchEvent) => Images.moveThumbsTouchMoveEvent(event, (event.target as HTMLElement).parentElement, this)}"
                        >
                            ${this.galleryImages.map((image, index) => html`
                                <img class="${this.currentImage === index ? 'selected' : ''}" src="${image.src}" alt="" @click="${(event: PointerEvent) => {
                                    event.stopPropagation()
                                    this.currentImage = index
                                    Images.isMagOn = false
                                    this.render()
                                }}" />
                            `)}
                        </div>
                    ` : nothing}
                </div>
            `,
            this.wrapper,
        )
    }
}
