import InjectSkin from './injectSkin.js'
import { html, render } from 'lit'

export default class Slider extends InjectSkin {
    private blocks: HTMLDivElement[]
    private autoScroll: number
    private counter: number
    private interval: number
    private isPaused: boolean = false

    get link() {
        return this.getAttribute('link')
    }

    get type() {
        return this.getAttribute('type')
    }

    async connectedCallback() {
        if (this.type === 'fullscreenCarousel') document.body.classList.add('is-fullscreen-carousel')
        this.blocks = Array.from(this.querySelectorAll('div')).sort(() => Math.random() - 0.5)
        this.autoScroll = parseInt(this.getAttribute('auto-scroll'))
        this.addEventListener('click', () => {
            location.href = this.link
        })
        this.innerHTML = ''
        this.render()
        this.slide()
        window.addEventListener('resize', () => {
            this.blocks.forEach((block) => this.setProperties(block))
            this.render()
        })
    }

    private render() {
        render(
            html`
                ${this.blocks.map((block) => block)}
                <div class="tools">
                    <button class="prev" @click="${(event: MouseEvent) => {
                        event.stopPropagation()
                        this.slide(this.counter - 2)
                    }}" title="￼Image précédente￼">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                            <path
                                d="M25.297 2.203c0.391-0.391 0.703-0.25 0.703 0.297v23c0 0.547-0.313 0.688-0.703 0.297l-11.094-11.094c-0.094-0.094-0.156-0.187-0.203-0.297v11.094c0 0.547-0.313 0.688-0.703 0.297l-11.094-11.094c-0.391-0.391-0.391-1.016 0-1.406l11.094-11.094c0.391-0.391 0.703-0.25 0.703 0.297v11.094c0.047-0.109 0.109-0.203 0.203-0.297z"></path>
                        </svg>
                    </button>
                    <button class="pause" @click="${(event: MouseEvent) => {
                        event.stopPropagation()
                        const button = event.currentTarget as HTMLButtonElement
                        this.isPaused = !this.isPaused
                        if (this.isPaused) {
                            this.blocks.forEach((block) => block.classList.remove(...['odd', 'even']))
                            button.className = 'play'
                            button.title = '￼Relancer la lecture du carousel￼'
                            clearInterval(this.interval)
                        } else {
                            button.className = 'pause'
                            button.title = '￼Mettre le carousel en pause￼'
                            this.slide(this.counter - 1)
                        }
                        this.render()
                    }}" title="￼Mettre le carousel en pause￼">
                        ${this.isPaused ? html`
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="28" viewBox="0 0 22 28">
                                <path d="M21.625 14.484l-20.75 11.531c-0.484 0.266-0.875 0.031-0.875-0.516v-23c0-0.547 0.391-0.781 0.875-0.516l20.75 11.531c0.484 0.266 0.484 0.703 0 0.969z"></path>
                            </svg>
                        ` : html`
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="28" viewBox="0 0 24 28">
                                <path
                                    d="M24 3v22c0 0.547-0.453 1-1 1h-8c-0.547 0-1-0.453-1-1v-22c0-0.547 0.453-1 1-1h8c0.547 0 1 0.453 1 1zM10 3v22c0 0.547-0.453 1-1 1h-8c-0.547 0-1-0.453-1-1v-22c0-0.547 0.453-1 1-1h8c0.547 0 1 0.453 1 1z"></path>
                            </svg>
                        `}
                    </button>
                    <button class="next" @click="${(event: MouseEvent) => {
                        event.stopPropagation()
                        this.slide(this.counter)
                    }}" title="￼Image suivante￼">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="28" viewBox="0 0 24 28">
                            <path
                                d="M0.703 25.797c-0.391 0.391-0.703 0.25-0.703-0.297v-23c0-0.547 0.313-0.688 0.703-0.297l11.094 11.094c0.094 0.094 0.156 0.187 0.203 0.297v-11.094c0-0.547 0.313-0.688 0.703-0.297l11.094 11.094c0.391 0.391 0.391 1.016 0 1.406l-11.094 11.094c-0.391 0.391-0.703 0.25-0.703-0.297v-11.094c-0.047 0.109-0.109 0.203-0.203 0.297z"></path>
                        </svg>
                    </button>
                </div>
            `,
            this,
        )
    }

    private slide(pCounter: number = null) {
        clearInterval(this.interval)
        let isForward = pCounter === null || pCounter === this.counter
        this.counter = pCounter || 0
        const transition = () => {
            if (this.blocks.length === this.counter) this.counter = 0
            if (isForward) {
                if (this.counter > 0) this.hide(this.blocks[this.counter - 1])
                if (this.counter === 0) this.hide(this.blocks[this.blocks.length - 1])
            } else {
                if (this.counter >= 0) this.hide(this.blocks[this.counter + 1])
                if (this.counter < 0) {
                    this.hide(this.blocks[0])
                    this.counter = this.blocks.length - 1
                }
            }
            const block = this.blocks[this.counter] as HTMLElement
            block.removeAttribute('hidden')
            block.classList.remove('not_visible')
            block.style.setProperty('--rand', `${Math.floor(Math.random() * (100 + 100 + 1) - 100)}px`)
            setTimeout(() => this.setProperties(block), 5)
            if (!this.isPaused) block.classList.add(block.matches(':nth-child(odd)') ? 'odd' : 'even')
            this.counter += 1
            isForward = true
        }
        this.interval = setInterval(transition, this.autoScroll)
        this.style.setProperty('--autoscroll', String(this.autoScroll))
        transition()
    }

    private setProperties(block: HTMLElement) {
        const image = block.firstChild as HTMLImageElement
        block.style.setProperty('--width', `${this.offsetWidth}px`)
        block.style.setProperty('--top', image.naturalHeight * this.offsetWidth / image.naturalWidth - this.offsetHeight >= this.offsetHeight ? '-40%' : 'auto')
    }

    private hide(pImage: Element) {
        pImage.classList.add('not_visible')
    }
}
