import l from"./injectSkin.js";import{Q as o,k as h}from"./index.js";class n extends l{blocks;autoScroll;counter;interval;isPaused=!1;get link(){return this.getAttribute("link")}get type(){return this.getAttribute("type")}async connectedCallback(){this.type==="fullscreenCarousel"&&document.body.classList.add("is-fullscreen-carousel"),this.blocks=Array.from(this.querySelectorAll("div")).sort(()=>Math.random()-.5),this.autoScroll=parseInt(this.getAttribute("auto-scroll")),this.addEventListener("click",()=>{location.href=this.link}),this.innerHTML="",this.render(),this.slide(),window.addEventListener("resize",()=>{this.blocks.forEach(t=>this.setProperties(t)),this.render()})}render(){o(h`
                ${this.blocks.map(t=>t)}
                <div class="tools">
                    <button class="prev" @click="${t=>{t.stopPropagation(),this.slide(this.counter-2)}}" title="￼Image précédente￼">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                            <path
                                d="M25.297 2.203c0.391-0.391 0.703-0.25 0.703 0.297v23c0 0.547-0.313 0.688-0.703 0.297l-11.094-11.094c-0.094-0.094-0.156-0.187-0.203-0.297v11.094c0 0.547-0.313 0.688-0.703 0.297l-11.094-11.094c-0.391-0.391-0.391-1.016 0-1.406l11.094-11.094c0.391-0.391 0.703-0.25 0.703 0.297v11.094c0.047-0.109 0.109-0.203 0.203-0.297z"></path>
                        </svg>
                    </button>
                    <button class="pause" @click="${t=>{t.stopPropagation();const s=t.currentTarget;this.isPaused=!this.isPaused,this.isPaused?(this.blocks.forEach(i=>i.classList.remove("odd","even")),s.className="play",s.title="￼Relancer la lecture du carousel￼",clearInterval(this.interval)):(s.className="pause",s.title="￼Mettre le carousel en pause￼",this.slide(this.counter-1)),this.render()}}" title="￼Mettre le carousel en pause￼">
                        ${this.isPaused?h`
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="28" viewBox="0 0 22 28">
                                <path d="M21.625 14.484l-20.75 11.531c-0.484 0.266-0.875 0.031-0.875-0.516v-23c0-0.547 0.391-0.781 0.875-0.516l20.75 11.531c0.484 0.266 0.484 0.703 0 0.969z"></path>
                            </svg>
                        `:h`
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="28" viewBox="0 0 24 28">
                                <path
                                    d="M24 3v22c0 0.547-0.453 1-1 1h-8c-0.547 0-1-0.453-1-1v-22c0-0.547 0.453-1 1-1h8c0.547 0 1 0.453 1 1zM10 3v22c0 0.547-0.453 1-1 1h-8c-0.547 0-1-0.453-1-1v-22c0-0.547 0.453-1 1-1h8c0.547 0 1 0.453 1 1z"></path>
                            </svg>
                        `}
                    </button>
                    <button class="next" @click="${t=>{t.stopPropagation(),this.slide(this.counter)}}" title="￼Image suivante￼">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="28" viewBox="0 0 24 28">
                            <path
                                d="M0.703 25.797c-0.391 0.391-0.703 0.25-0.703-0.297v-23c0-0.547 0.313-0.688 0.703-0.297l11.094 11.094c0.094 0.094 0.156 0.187 0.203 0.297v-11.094c0-0.547 0.313-0.688 0.703-0.297l11.094 11.094c0.391 0.391 0.391 1.016 0 1.406l-11.094 11.094c-0.391 0.391-0.703 0.25-0.703-0.297v-11.094c-0.047 0.109-0.109 0.203-0.203 0.297z"></path>
                        </svg>
                    </button>
                </div>
            `,this)}slide(t=null){clearInterval(this.interval);let s=t===null||t===this.counter;this.counter=t||0;const i=()=>{this.blocks.length===this.counter&&(this.counter=0),s?(this.counter>0&&this.hide(this.blocks[this.counter-1]),this.counter===0&&this.hide(this.blocks[this.blocks.length-1])):(this.counter>=0&&this.hide(this.blocks[this.counter+1]),this.counter<0&&(this.hide(this.blocks[0]),this.counter=this.blocks.length-1));const e=this.blocks[this.counter];e.removeAttribute("hidden"),e.classList.remove("not_visible"),e.style.setProperty("--rand",`${Math.floor(Math.random()*201-100)}px`),setTimeout(()=>this.setProperties(e),5),this.isPaused||e.classList.add(e.matches(":nth-child(odd)")?"odd":"even"),this.counter+=1,s=!0};this.interval=setInterval(i,this.autoScroll),this.style.setProperty("--autoscroll",String(this.autoScroll)),i()}setProperties(t){const s=t.firstChild;t.style.setProperty("--width",`${this.offsetWidth}px`),t.style.setProperty("--top",s.naturalHeight*this.offsetWidth/s.naturalWidth-this.offsetHeight>=this.offsetHeight?"-40%":"auto")}hide(t){t.classList.add("not_visible")}}export{n as default};
