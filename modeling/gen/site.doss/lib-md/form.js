import{Q as l,k as c}from"./index.js";import m from"./injectSkin.js";class d extends m{form;connectedCallback(){const i=this.getAttribute("email"),o=this.getAttribute("subject");this.removeAttribute("email"),this.removeAttribute("subject"),this.form=this.querySelector("form"),this.render();const s=this.form.querySelectorAll("input,textarea");this.form.addEventListener("submit",async n=>{n.preventDefault();let e="";s.forEach(r=>{let t=r.parentElement.querySelector("label").innerHTML;t=t.includes(" *")?t.slice(0,-2):t,e+=`${t}: ${r.value}
`}),location.href=`mailto:${i}?subject=${o}&body=${encodeURIComponent(e)}`})}render(){l(c`
                <div class="submitButton">
                    <button type="submit">￼Envoyer￼</button>
                </div>
            `,this.form)}}export{d as default};
