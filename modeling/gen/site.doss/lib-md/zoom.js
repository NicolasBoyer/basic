import{U as r,Q as a,k as o,D as n}from"./index.js";import{I as s}from"./images.js";class c extends HTMLAnchorElement{isHidden=!0;isMag;wrapper;constructor(){super(),r.declareSkin(this.skin),this.isMag=this.mag==="yes";const e=new Image;e.onload=()=>{s.zoomImages[this.href]={height:e.naturalHeight,width:e.naturalWidth}},e.src=this.href,this.addEventListener("click",t=>{t.preventDefault(),this.isHidden=!1,document.body.style.overflow="hidden",this.render(),this.wrapper.style.display="",s.controller=new AbortController,document.body.addEventListener("keyup",i=>this.setKeyboardEvent(i)),setTimeout(()=>this.resizeZoomedImage(),50)}),this.wrapper=document.createElement("div"),this.wrapper.style.display="none",document.body.appendChild(this.wrapper),this.render(),window.addEventListener("resize",()=>this.resizeZoomedImage())}get skin(){return this.getAttribute("skin")}get mag(){return this.getAttribute("mag")}setKeyboardEvent(e){e.key==="Escape"&&(this.isHidden=!0,s.close(),this.render())}resizeZoomedImage(){s.resize(this.wrapper,this.wrapper.querySelector(".images img")),this.render()}render(){a(o`
                <div class="container" ?hidden="${this.isHidden}" @click="${()=>{this.isHidden=!0,s.close(),this.render()}}">
                    <div class="hover"></div>
                    <div class="tools">
                        <button class="close" role="button" title="￼Fermer la fenêtr (Echap)￼" @click="${e=>{e.stopPropagation(),this.isHidden=!0,s.close(),this.render()}}" aria-label="￼Fermer la fenêtre￼">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"
                            ">
                            <path
                                d="M31.708 25.708c-0-0-0-0-0-0l-9.708-9.708 9.708-9.708c0-0 0-0 0-0 0.105-0.105 0.18-0.227 0.229-0.357 0.133-0.356 0.057-0.771-0.229-1.057l-4.586-4.586c-0.286-0.286-0.702-0.361-1.057-0.229-0.13 0.048-0.252 0.124-0.357 0.228 0 0-0 0-0 0l-9.708 9.708-9.708-9.708c-0-0-0-0-0-0-0.105-0.104-0.227-0.18-0.357-0.228-0.356-0.133-0.771-0.057-1.057 0.229l-4.586 4.586c-0.286 0.286-0.361 0.702-0.229 1.057 0.049 0.13 0.124 0.252 0.229 0.357 0 0 0 0 0 0l9.708 9.708-9.708 9.708c-0 0-0 0-0 0-0.104 0.105-0.18 0.227-0.229 0.357-0.133 0.355-0.057 0.771 0.229 1.057l4.586 4.586c0.286 0.286 0.702 0.361 1.057 0.229 0.13-0.049 0.252-0.124 0.357-0.229 0-0 0-0 0-0l9.708-9.708 9.708 9.708c0 0 0 0 0 0 0.105 0.105 0.227 0.18 0.357 0.229 0.356 0.133 0.771 0.057 1.057-0.229l4.586-4.586c0.286-0.286 0.362-0.702 0.229-1.057-0.049-0.13-0.124-0.252-0.229-0.357z"></path>
                            </svg>
                        </button>
                    </div>
                    <div class="images">
                        <div
                            @mousemove="${s.isMagOn?e=>s.magMouseMove(e):n}"
                            class="${this.isMag&&s.zoomImages[this.href]?.isMag?"isMag":""} ${s.isMagOn?"isMagOn":""}"
                            style="${s.isMagOn?`background-image:url(${this.href})`:""}"
                            @click="${e=>{e.stopPropagation(),s.setMag(this.href,this.isMag),this.render()}}"
                        >
                            <img style="display: none" src="${this.href}" alt="" />
                        </div>
                    </div>
                </div>
            `,this.wrapper)}}export{c as default};
