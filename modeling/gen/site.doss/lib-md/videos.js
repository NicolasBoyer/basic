import{Q as s,k as t,_ as o}from"./index.js";import d from"./injectSkin.js";class l extends d{videos=JSON.parse(this.firstChild.textContent);constructor(){super(),this.firstChild.remove(),this.videos.length===1&&this.classList.add("oneVideo"),s(this.videos.map((e,i)=>t`
                ${e.u.includes("<iframe")?t`
                    <div class="iframe">${document.createRange().createContextualFragment(e.u)}</div>
                `:t`
                    <video id="video_${i}" src="${e.u}"></video>
                `}
            `),this),this.initVideos()}async initVideos(){const e=await o(()=>import("./vlite.js"),[],import.meta.url);for(let i=0;i<this.videos.length;i++)this.videos[i].u.includes("<iframe")||new e.default(`#video_${i}`)}}export{l as default};
