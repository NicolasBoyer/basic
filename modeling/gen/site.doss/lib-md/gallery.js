import d from"./injectSkin.js";import{Q as m,k as l,D as o}from"./index.js";import{I as s}from"./images.js";class n extends d{static isAdaptedImage=!0;static customGallery={};isHidden=!0;links=[];galleryImages=[];currentImage;galleryImageHeight={};imageGalleryPadding=6;touchstartX=0;touchendX=0;wrapper;isThumbs;isMag;get thumbs(){return this.getAttribute("thumbs")}get mag(){return this.getAttribute("mag")}get refGallery(){return this.getAttribute("refGallery")}connectedCallback(){s.thumbsGallery[String(this)]={touchstartX:0,touchendX:0,imagesTranslate:[]},this.isThumbs=this.thumbs==="yes",this.isMag=this.mag==="yes",(this.refGallery&&n.customGallery[this.refGallery]||this.querySelectorAll("a")).forEach((e,i)=>{this.galleryImages.push(e.querySelector("img")),this.links.push(e);const t=new Image;t.onload=()=>{s.zoomImages[e.href]={height:t.naturalHeight,width:t.naturalWidth}},t.src=e.href,e.addEventListener("click",a=>{a.preventDefault(),this.currentImage=i,this.isHidden=!1,document.body.style.overflow="hidden",this.render(),this.wrapper.style.display="",s.controller=new AbortController,document.body.addEventListener("keyup",h=>this.setKeyboardEvent(h)),setTimeout(()=>{this.resizeZoomedImages(),this.selectThumbInView()},50)})}),window.addEventListener("imagesVisible",()=>this.initGallerySize()),this.wrapper=document.querySelector(`#wrapper${this.refGallery}`),this.wrapper||(this.wrapper=document.createElement("div"),this.refGallery&&(this.wrapper.id=`wrapper${this.refGallery}`),this.wrapper.style.display="none",document.body.appendChild(this.wrapper)),this.render(),this.style.setProperty("--imageGalleryPadding",`${this.imageGalleryPadding/2-1}px`),window.addEventListener("resize",()=>{this.initGallerySize(),this.resizeZoomedImages()})}initGallerySize(){const e=document.querySelector(".gallery");if(!n.isAdaptedImage||!e)return;const i=e.getBoundingClientRect().width;let t=0,a=0,h=0;this.galleryImages.forEach((r,c)=>{if(t+=(r.naturalWidth-this.imageGalleryPadding)/(r.naturalHeight-this.imageGalleryPadding),h+=r.naturalWidth-this.imageGalleryPadding,a++,h>=i){for(let g=c-(a-1);g<=c;g++)this.galleryImageHeight[this.galleryImages[g].src]=(i-a-this.imageGalleryPadding)/t;t=0,h=0,a=0}}),this.galleryImages.forEach(r=>{this.galleryImageHeight[r.src]?(r.style.height=this.galleryImageHeight[r.src]+"px",r.style.width="auto"):r.style.height="auto"})}resizeZoomedImages(e=!1){const i=this.wrapper.querySelectorAll(".images img");this.render(),e?s.resize(this.wrapper,i[this.currentImage]):i.forEach(async t=>s.resize(this.wrapper,t))}setKeyboardEvent(e){e.key==="Escape"&&(this.isHidden=!0,s.close(),this.render()),e.key==="ArrowLeft"&&this.left(),e.key==="ArrowRight"&&this.right()}selectThumbInView(){document.querySelector(".thumbs > img.selected")?.scrollIntoView()}left(){this.currentImage>0&&(this.currentImage=this.currentImage-1),s.isMagOn=!1,this.resizeZoomedImages(!0),this.selectThumbInView()}right(){this.currentImage<this.galleryImages.length-1&&(this.currentImage=this.currentImage+1),s.isMagOn=!1,this.resizeZoomedImages(!0),this.selectThumbInView()}render(){m(l`
                <div class="container" ?hidden="${this.isHidden}" @click="${()=>{this.isHidden=!0,s.close(),this.render()}}">
                    <div class="hover"></div>
                    <div class="tools">
                        <button class="close" role="button" title="￼Fermer la fenêtre (Echap)￼" @click="${e=>{e.stopPropagation(),this.isHidden=!0,s.close(),this.render()}}" aria-label="￼Fermer la fenêtre￼">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"
                            ">
                            <path
                                d="M31.708 25.708c-0-0-0-0-0-0l-9.708-9.708 9.708-9.708c0-0 0-0 0-0 0.105-0.105 0.18-0.227 0.229-0.357 0.133-0.356 0.057-0.771-0.229-1.057l-4.586-4.586c-0.286-0.286-0.702-0.361-1.057-0.229-0.13 0.048-0.252 0.124-0.357 0.228 0 0-0 0-0 0l-9.708 9.708-9.708-9.708c-0-0-0-0-0-0-0.105-0.104-0.227-0.18-0.357-0.228-0.356-0.133-0.771-0.057-1.057 0.229l-4.586 4.586c-0.286 0.286-0.361 0.702-0.229 1.057 0.049 0.13 0.124 0.252 0.229 0.357 0 0 0 0 0 0l9.708 9.708-9.708 9.708c-0 0-0 0-0 0-0.104 0.105-0.18 0.227-0.229 0.357-0.133 0.355-0.057 0.771 0.229 1.057l4.586 4.586c0.286 0.286 0.702 0.361 1.057 0.229 0.13-0.049 0.252-0.124 0.357-0.229 0-0 0-0 0-0l9.708-9.708 9.708 9.708c0 0 0 0 0 0 0.105 0.105 0.227 0.18 0.357 0.229 0.356 0.133 0.771 0.057 1.057-0.229l4.586-4.586c0.286-0.286 0.362-0.702 0.229-1.057-0.049-0.13-0.124-0.252-0.229-0.357z"></path>
                            </svg>
                        </button>
                        ${this.currentImage>0?l`
                            <button class="left" role="button" title="￼Image précéndente (Flêche gauche)￼" @click="${e=>{e.stopPropagation(),this.left()}}" aria-label="￼Image précéndente￼">
                                <svg xmlns="http://www.w3.org/2000/svg" width="21" height="28" viewBox="0 0 21 28">
                                    <path
                                        d="M18.297 4.703l-8.297 8.297 8.297 8.297c0.391 0.391 0.391 1.016 0 1.406l-2.594 2.594c-0.391 0.391-1.016 0.391-1.406 0l-11.594-11.594c-0.391-0.391-0.391-1.016 0-1.406l11.594-11.594c0.391-0.391 1.016-0.391 1.406 0l2.594 2.594c0.391 0.391 0.391 1.016 0 1.406z"></path>
                                </svg>
                            </button>
                        `:o}
                        ${this.currentImage<this.galleryImages.length-1?l`
                            <button class="right" role="button" title="￼Image suivante (Flêche droite)￼" @click="${e=>{e.stopPropagation(),this.right()}}" aria-label="￼Image suivante￼">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="28" viewBox="0 0 19 28">
                                    <path
                                        d="M17.297 13.703l-11.594 11.594c-0.391 0.391-1.016 0.391-1.406 0l-2.594-2.594c-0.391-0.391-0.391-1.016 0-1.406l8.297-8.297-8.297-8.297c-0.391-0.391-0.391-1.016 0-1.406l2.594-2.594c0.391-0.391 1.016-0.391 1.406 0l11.594 11.594c0.391 0.391 0.391 1.016 0 1.406z"></path>
                                </svg>
                            </button>
                        `:o}
                    </div>
                    <div class="images">
                        ${this.links.map((e,i)=>l`
                            <div
                                @mousemove="${s.isMagOn?t=>s.magMouseMove(t):o}"
                                class="${this.isMag&&s.zoomImages[e.href]?.isMag?"isMag":""} ${s.isMagOn?"isMagOn":""}"
                                style="${s.isMagOn?`background-image:url(${e.href})`:""}"
                                @touchstart="${t=>{this.touchstartX=t.changedTouches[0].screenX}}"
                                @touchend="${t=>{this.touchendX=t.changedTouches[0].screenX,this.touchendX<this.touchstartX&&this.right(),this.touchendX>this.touchstartX&&this.left()}}"
                                @click="${t=>{t.stopPropagation(),s.setMag(e.href,this.isMag),this.render()}}"
                            >
                                <img ?hidden="${this.currentImage!==i}" style="display: none" src="${e.href}" alt="${e.querySelector("img").alt}" />
                                <div ?hidden="${this.currentImage!==i}" class="legend">${e.title}</div>
                            </div>
                        `)}
                    </div>
                    ${this.isThumbs?l`
                        <div class="thumbs"
                             @wheel="${e=>s.moveThumbsWheelEvent(e)}"
                             @touchstart="${e=>s.moveThumbsTouchStartEvent(e,e.target.parentElement,this)}"
                             @touchmove="${e=>s.moveThumbsTouchMoveEvent(e,e.target.parentElement,this)}"
                        >
                            ${this.galleryImages.map((e,i)=>l`
                                <img class="${this.currentImage===i?"selected":""}" src="${e.src}" alt="" @click="${t=>{t.stopPropagation(),this.currentImage=i,s.isMagOn=!1,this.render()}}" />
                            `)}
                        </div>
                    `:o}
                </div>
            `,this.wrapper)}}export{n as default};
