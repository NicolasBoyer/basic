import{Q as l,k as s}from"./index.js";class a extends HTMLDivElement{titles=[];connectedCallback(){document.querySelectorAll(".section").forEach(e=>{const t=e.querySelector("h2");t&&this.titles.push(t)}),!(this.titles.length<=1)&&this.buildMenu()}buildMenu(){this.titles.forEach((i,e)=>i.setAttribute("id",`section_${e}`)),l(s`
                <nav id="sectionMenu" aria-label="menu interne de la page">
                    <ul>
                        ${this.titles.map((i,e)=>{const t=`#section_${e}`;return s`
                                <li>
                                    <a href="${t}" @click="${o=>this.gotoSection(t,o)}">
                                        <span>${i.innerText}</span>
                                    </a>
                                </li>
                            `})}
                    </ul>
                </nav>
            `,this)}gotoSection(i,e){e.preventDefault();const t=document.querySelector("#header"),o=document.querySelector(i),n=o.style.top;o.style.position="relative",o.style.top=getComputedStyle(t).position==="static"?"0":`-${t.offsetHeight}px`,o.scrollIntoView({behavior:"smooth"}),o.style.top=n}}export{a as default};
