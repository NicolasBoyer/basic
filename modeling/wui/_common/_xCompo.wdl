<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:anyCompositionPrim/>
	<sm:editPoints>
		<sm:tag refCodes="*">
			<sm:headBodyWidget collapsed="forbidden">
				<sm:container>
					<sm:cssRules xml:space="preserve">:host{
  border-inline-start: none !important;
  margin-block:.3em; 
}

:host .head{
  background-color: var(--alt2-bgcolor);
  border-radius:.3em;
  padding:.3em;
}

:host .head .label{
  font-weight:bold;
  font-variant:small-caps;
}</sm:cssRules>
				</sm:container>
				<sm:callMetaAndSubModel/>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>
