<?xml version="1.0"?>
<sm:textWdl xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:anyTextPrim/>
	<sm:editPoints>
		<sm:root/>
		<sm:blockTag tag="para">
			<sm:htmlStyle>
				<sm:entryStyle key="padding" value=".3em"/>
				<sm:entryStyle key="line-height" value="1.25em"/>
			</sm:htmlStyle>
		</sm:blockTag>
		<sm:blockTag tag="itemizedList">
			<sm:behaviors>
				<sm:keyBinding char="*"/>
			</sm:behaviors>
			<sm:subBlockTag tag="listItem">
				<sm:htmlStyle>
					<sm:entryStyle key="margin-inline-start" value="1.2em"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:htmlStyle>
				<sm:entryStyle key="margin-inline-start" value="15px"/>
				<sm:entryStyle key="list-style" value="disc"/>
			</sm:htmlStyle>
		</sm:blockTag>
		<sm:blockTag tag="orderedList">
			<sm:behaviors>
				<sm:keyBinding char="1"/>
			</sm:behaviors>
			<sm:subBlockTag tag="listItem">
				<sm:htmlStyle>
					<sm:entryStyle key="margin-inline-start" value="1.2em"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:htmlStyle>
				<sm:entryStyle key="margin-inline-start" value="12px"/>
				<sm:entryStyle key="list-style" value="decimal"/>
			</sm:htmlStyle>
		</sm:blockTag>
		<sm:blockTag tag="table">
			<sm:toolbar>
				<sm:button group="gpBlock" sortKey="100">
					<sm:icon sc:refUri="/wui/_res/mn_tables.svg"/>
				</sm:button>
			</sm:toolbar>
			<sm:subBlockTag tag="cell">
				<sm:htmlStyle>
					<sm:entryStyle key="border" value="1px solid var(--edit-color)"/>
					<sm:entryStyle key="color" value="var(--edit-color)"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag role="word" tag="cell">
				<sm:htmlStyle>
					<sm:entryStyle key="text-align" value="center"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag role="num" tag="cell">
				<sm:htmlStyle>
					<sm:entryStyle key="text-align" value="end"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag role="head" tag="row">
				<sm:htmlStyle>
					<sm:entryStyle key="background-color" value="var(--alt1-bgcolor)"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag role="head" tag="column">
				<sm:htmlStyle>
					<sm:entryStyle key="background-color" value="var(--alt1-bgcolor)"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="caption">
				<sm:htmlStyle>
					<sm:entryStyle key="color" value="gray"/>
					<sm:entryStyle key="text-align" value="center"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
		</sm:blockTag>
		<sm:inlineTag role="url" tag="phrase">
			<sm:behaviors>
				<sm:keyBinding char="U"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="direction" value="ltr"/>
				<sm:entryStyle key="text-decoration" value="underline"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
			<sm:handler>
				<sm:addAttribute name="title">
					<sm:dynamicString>
						<sm:freeMetaSelect querySelector="url"/>
					</sm:dynamicString>
				</sm:addAttribute>
			</sm:handler>
		</sm:inlineTag>
		<sm:inlineTag role="quote" tag="phrase">
			<sm:behaviors>
				<sm:keyBinding char="I"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-family" value="&quot;Times New Roman&quot;, Times, serif"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="before">
				<sm:entryStyle key="font-weight" value="normal"/>
				<sm:entryStyle key="content" value="&quot;\&quot;&quot;"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="after">
				<sm:entryStyle key="font-weight" value="normal"/>
				<sm:entryStyle key="content" value="&quot;\&quot;&quot;"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag role="emp" tag="inlineStyle">
			<sm:behaviors>
				<sm:keyBinding char="E"/>
			</sm:behaviors>
			<sm:toolbar>
				<sm:button group="inlTags" sortKey="10">
					<sm:icon sc:refUri="/wui/_res/emphasis.svg"/>
				</sm:button>
			</sm:toolbar>
			<sm:htmlStyle>
				<sm:entryStyle key="font-weight" value="bold"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag role="spec" tag="inlineStyle">
			<sm:behaviors>
				<sm:keyBinding char="P"/>
			</sm:behaviors>
			<sm:toolbar>
				<sm:button group="inlTags" sortKey="20">
					<sm:icon sc:refUri="/wui/_res/specific.svg"/>
				</sm:button>
			</sm:toolbar>
			<sm:htmlStyle>
				<sm:entryStyle key="font-style" value="italic"/>
			</sm:htmlStyle>
			<sm:handler>
				<sm:addAttribute name="lang">
					<sm:dynamicString>
						<sm:freeMetaSelect querySelector="code"/>
					</sm:dynamicString>
				</sm:addAttribute>
				<sm:addAttribute name="title">
					<sm:dynamicString>
						<sm:freeMetaSelect defaultValue="￼;Langue non spécifiée￼" prefix="￼;Langue : ￼" querySelector="code"/>
					</sm:dynamicString>
				</sm:addAttribute>
			</sm:handler>
		</sm:inlineTag>
		<sm:inlineTag role="sup" tag="inlineStyle">
			<sm:behaviors>
				<sm:keyBinding char="&lt;"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-size" value="70%"/>
				<sm:entryStyle key="vertical-align" value="4px"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag role="sub" tag="inlineStyle">
			<sm:behaviors>
				<sm:keyBinding char="_"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-size" value="70%"/>
				<sm:entryStyle key="vertical-align" value="-4px"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag role="ico" tag="inlineImg">
			<sm:toolbar>
				<sm:button refMenuId="mnInline"/>
			</sm:toolbar>
		</sm:inlineTag>
	</sm:editPoints>
</sm:textWdl>
