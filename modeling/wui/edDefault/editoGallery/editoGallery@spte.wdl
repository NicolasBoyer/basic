<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:wed="scenari.eu:wed">
	<sm:model sc:refUri="/model/objects/editoGallery.model"/>
	<sm:axis code="spte"/>
	<sm:editPoints>
		<sm:root>
			<sm:openEdtWidget>
				<sm:content wedlet="Offscreen">
					<sm:call>
						<sm:meta axis="spte"/>
					</sm:call>
					<sm:call>
						<sm:parts axisParts="#current" partsCodes="zone"/>
					</sm:call>
				</sm:content>
			</sm:openEdtWidget>
		</sm:root>
		<sm:tag refCodes="zone">
			<sm:openEdtWidget>
				<sm:content wedlet="SpTeSegment">
					<wed:children callModes="spte" select="sc:spatial">
						<wed:bind eltName="sc:spatial" wedlet="SpTeSpatial"/>
					</wed:children>
				</sm:content>
			</sm:openEdtWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>
