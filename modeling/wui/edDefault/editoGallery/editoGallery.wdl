<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns="http://www.w3.org/1999/xhtml" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/model/objects/editoGallery.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:openEdtWidget>
				<sm:wedJsCoreLib modelingKey="spte-wallpaper"/>
				<sm:freeCallModel axis="spte" sc:refUri="/model/objects/editoGallery.model"/>
				<sm:freeCallModel axis="spatial" sc:refUri="/model/objects/editoGallery.model"/>
				<sm:content wedlet="BoxSpTeRoot">
					<box-ctn class="sm-root labelRoot" skin="box/head-body" skinOver="box/head-body/float" wed-name="ba:editoGallery#">
						<div class="body">
							<style>#spteEditor {
	--structureColumnWidth: var(--structureColumnWidthSmall);
	display: flex;
	margin-top: 0.5em;
	min-height: 0;
	min-width: 0;
	border: 1px solid var(--border-color);
}
spte-wallpaper {
	flex: 1 1 50%;
	position: sticky;
	top: 0;
	min-width: 15em;
}
c-resizer {
	width: 1px;
}
#zones {
	flex: 1 1 50%;
	flex-direction: column;
	padding-inline-start: .2em;
	padding-inline-end: .2em;
}</style>
							<div class="metas">
								<sm:call>
									<sm:meta/>
								</sm:call>
							</div>
							<div id="spteEditor">
								<div c-resizable="" id="zones">
									<sm:call>
										<sm:parts axisParts="#current" partsCodes="zone"/>
									</sm:call>
								</div>
								<c-resizer c-orient="row"/>
								<spte-wallpaper c-resizable="" creatable-shapes="rect" wedlet-modes="spte"/>
							</div>
						</div>
					</box-ctn>
				</sm:content>
			</sm:openEdtWidget>
		</sm:root>
		<sm:tag refCodes="zone">
			<sm:headBodyWidget collapsed="allowed" collapsedMode="start-switch" layout="float">
				<sm:container class="sm-sub-level labelStruct" style="margin-top:0.2em !important; margin-bottom:0.2em !important;">
					<sm:cssRules xml:space="preserve">div.body {
  padding-inline-start: 0;
}</sm:cssRules>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>
