<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:wed="scenari.eu:wed">
	<sm:model sc:refUri="/model/objects/editoGalleryM.model"/>
	<sm:axis code="spte"/>
	<sm:editPoints>
		<sm:root>
			<sm:openEdtWidget>
				<sm:content wedlet="Offscreen">
					<sm:call>
						<sm:members axis="#current" membersCodes="img"/>
					</sm:call>
				</sm:content>
			</sm:openEdtWidget>
		</sm:root>
		<sm:tag refCodes="img">
			<sm:openEdtWidget>
				<sm:content wedlet="Offscreen">
					<wed:children select="@sc:refUri">
						<wed:bind attName="sc:refUri" wedlet="SpTeSourceItem"/>
					</wed:children>
				</sm:content>
			</sm:openEdtWidget>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>
