<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/model/objects/editoGalleryM.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:skippedWidget layout="unspecified"/>
		</sm:root>
		<sm:tag refCodes="img">
			<sm:headBodyWidget collapsed="forbidden" layout="horizontal">
				<sm:container class="labelStruct"/>
				<sm:callRefItemField layout="compact"/>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>
