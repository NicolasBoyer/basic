export async function jslibAsyncInit(jsEndPoint) {
    const { DOM } = await jsEndPoint.importJs(":lib:commons/xml/dom.js");
    if (!customElements.get('ba-box-xviewtext-ratio-enum')) {
        class BoxXViewTextsRatioEnum extends customElements.get("box-input-enum") {
            get wedType() {
                return this.getAttribute('wed-type');
            }
            set wedType(pName) {
                this.setAttribute('wed-type', pName);
            }
            connectedCallback() {
                super.connectedCallback();
                if (this.binded)
                    this.updateTxtRes(this.getKey());
            }
            refreshBindValue(val) {
                super.refreshBindValue(val);
                this.binded = true;
                if (this.isConnected)
                    this.updateTxtRes(val);
            }
            updateTxtRes(ratio) {
                if (!this.txtBox) {
                    this.txtBox = DOM.findParent(this, null, (n) => {
                        return DOM.IS_element(n) && (n.getAttribute('wed-name') == this.wedType);
                    });
                }
                if (this.txtBox) {
                    if (ratio)
                        this.txtBox.setAttribute('ratio', ratio);
                    else
                        this.txtBox.removeAttribute('ratio');
                }
            }
        }
        customElements.define('ba-box-xviewtext-ratio-enum', BoxXViewTextsRatioEnum);
    }
}