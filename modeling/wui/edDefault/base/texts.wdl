<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/model/objects/texts.model"/>
	<sm:editPoints>
		<sm:tag refCodes="info">
			<sm:headBodyWidget>
				<sm:container class="labelBase">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/_res/information.svg"/>
						<sm:name style="margin-left:.3em;"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="visavis">
			<sm:headBodyWidget>
				<sm:container class="labelBase">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/_res/visavis.svg"/>
						<sm:name style="margin-left:.3em;"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="emphasis">
			<sm:headBodyWidget>
				<sm:container class="labelBase" style="background-color:var(--alt1-bgcolor);">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/_res/important.svg"/>
						<sm:name style="margin-left:.3em;"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>
