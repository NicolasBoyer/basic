<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns="http://www.w3.org/1999/xhtml" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/model/base/visavis.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:openEdtWidget>
				<sm:widgetLib sc:refUri="/wui/edDefault/base/_xViewTextsM.doss">
					<sm:jsFile fileName="xViewTextsM.js" key="XVIEWTEXTSM"/>
				</sm:widgetLib>
				<sm:contentBox>
					<sm:cssRules xml:space="preserve">:host .grid {
	display: grid;
	grid-column-gap: 5px;
}

:host([wed-name='ba_visavis#']) .grid {
	grid-template-columns: auto auto;
}

:host([wed-name='ba_visavis#'][ratio='half-half']) .grid {
	grid-template-columns: 50% 50%;
}

:host([wed-name='ba_visavis#'][ratio='one-two']) .grid {
	grid-template-columns: 33% 66%;
}

:host([wed-name='ba_visavis#'][ratio='two-one']) .grid {
	grid-template-columns: 66% 33%;
}

:host([wed-name='ba_visavis#'][ratio='one-three']) .grid {
	grid-template-columns: 25% 75%;
}

:host([wed-name='ba_visavis#'][ratio='three-one']) .grid {
	grid-template-columns: 75% 25%;
}

:host([wed-name='ba_visavis#'][ratio='auto']) .grid {
	grid-template-columns: auto auto;
}</sm:cssRules>
					<sm:call>
						<sm:meta/>
					</sm:call>
					<div class="grid">
						<sm:call>
							<sm:parts axisParts="#current" partsCodes="first next"/>
						</sm:call>
					</div>
				</sm:contentBox>
			</sm:openEdtWidget>
		</sm:root>
		<sm:tag refCodes="first next">
			<sm:skippedWidget focusable="true"/>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>
