<?xml version="1.0"?>
<sm:freeWdl xmlns="http://www.w3.org/1999/xhtml" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:wed="scenari.eu:wed">
	<sm:model sc:refUri="/model/base/visavisM.model"/>
	<sm:wedlingLib libId="xViewTextsM" sc:refUri="/wui/edDefault/base/_xViewTextsM.doss">
		<sm:jsFile fileName="xViewTextsM.js" key="XVIEWTEXTSM"/>
	</sm:wedlingLib>
	<sm:bindsRegistry>
		<wed:bind eltName="ba:visavisM" wedlet="Box">
			<box-ctn class="v sm-content" skin="box/skipped" wed-name="ba_visavisM#">
				<wed:slot>
					<wed:children select="sp:ratio">
						<wed:display eltName="sp:ratio"/>
						<wed:bind eltName="sp:ratio" label="Largeurs" wedlet="Box">
							<box-ctn class="h sm-property" hv="auto" skin="box/head-body" skinOver="box/head-body/block custom-property-field" wed-name="ratio#">
								<div class="head">
									<box-label class="label">
										<sm:addAttribute name="title">
											<sm:extractFromModelCtx>
												<sm:quickHelp/>
											</sm:extractFromModelCtx>
										</sm:addAttribute>
									</box-label>
								</div>
								<div class="body">
									<wed:children>
										<wed:display nodeType="text" wedlet="Box">
											<ba-box-xviewtext-ratio-enum wed-name="ratio#-input" wed-type="ba_visavis#">
												<e k="half-half" v="1/2 - 1/2"/>
												<e k="one-two" v="1/3 - 2/3"/>
												<e k="two-one" v="2/3 - 1/3"/>
												<e k="one-three" v="1/4 - 3/4"/>
												<e k="three-one" v="3/4 - 1/4"/>
												<sep/>
												<e k="auto" v="Adapté au contenu"/>
											</ba-box-xviewtext-ratio-enum>
										</wed:display>
									</wed:children>
								</div>
							</box-ctn>
						</wed:bind>
					</wed:children>
				</wed:slot>
			</box-ctn>
		</wed:bind>
	</sm:bindsRegistry>
</sm:freeWdl>
