<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/model/resources/videoM.model"/>
	<sm:editPoints>
		<sm:tag refCodes="accessibility">
			<sm:headBodyWidget>
				<sm:container class="labelBase"/>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>
